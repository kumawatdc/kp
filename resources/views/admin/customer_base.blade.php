@include('layouts.left-side')
<div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Customer base</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Customer base</li>
                        </ol>
                        <div class="row">
                        <div class="col-xl-6 col-md-6 mb-4">
                              <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                  <div class="row no-gutters align-items-center">
                                    <table class="table table-bordered" id="" width="100%" cellspacing="0">
                                      <thead>
                                        <tr>
                                          <th>ID NUMBER</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php
                                        $i = 0;
                                        foreach ($pages as $key => $value) {
                                        $i++;
                                        ?>
                                          <tr>
                                              <td><a href="view-customer/<?php echo $value->id?>"><?php echo $value->id; ?></a></td>
                                          </tr>
                                        <?php
                                        }?>
                                      </tbody>
                                    </table>
                                     <div class="col-md-12"> 
                                         {{ $pages->links() }} 
                                    </div>
                                  </div>
                                </div>
                              </div>
                        </div>
 
                    </div>
                </main>
@include('layouts.footer')