@include('layouts.left-side')

<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
          <a class="btn btn-primary mt-4" href="<?php echo url('/'); ?>/admin/new-videos">Add New Video</a>
            <h1 class="mt-4">Videos</h1>
            <!-- <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Customer base</li>
            </ol> -->
            <div class="row">
            <div class="col-xl-12 col-md-12 mb-4">
                  <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                      <div class="row no-gutters align-items-center">
                        
                        <form method="post" action="<?php echo url('/'); ?>/admin/update-video" enctype="multipart/form-data" role="form" id="page-form">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="id" value="<?php echo $pages->id; ?>">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input type="text" class="form-control required" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Title" value="<?php echo $pages->title; ?>" name="title" required="">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Album</label>
                            <select class="form-control" id="gallery" name="gallery" required="">
                              <option value="">Please Select Album</option>
                              <?php foreach ($gallery as $key => $value) { ?>
                                    <option value="<?php echo @$value->id; ?>" <?php if($pages->gallery_id == $value->id) { echo "Selected"; } ?>><?php echo @$value->title; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="form-group">
                            <input type="hidden" name="downloading" value="0" />
                            <input type="checkbox" name="downloading" value="1" <?php if($pages->download_satus == '1'){ echo "checked"; } ?>> Possibility for Downloading
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlFile1">Image</label>
                            <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                            <input type="hidden" name="old_image" value="<?php echo $pages->photo; ?>">
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlFile1">Video</label>
                            <input type="file" name="video" class="form-control-file" id="exampleFormControlFile1">
                            <input type="hidden" name="old_video" value="<?php echo $pages->video; ?>">
                          </div>
                          <div class="col-md-6 col-sm-4 text-center">
                              <?php if(!empty($pages->photo)){ ?> 
                                <img src="<?php echo url('/'); ?>/gallery/<?php echo $pages->photo; ?>" class="img-fluid" width="150">
                                <?php } else{ ?>
                                <img src="<?php echo url('/'); ?>/gallery/default.png" alt="" class="img-fluid" width="150">
                                <?php
                              }?>
                          </div> 
                          <div class="col-md-6 col-sm-4 text-center">
                            <?php if(!empty($pages->video)){ ?>
                              <video width="400" controls>
                                <source src="<?php echo url('/'); ?>/gallery/<?php echo $pages->video; ?>" type="video/mp4">
                                Your browser does not support HTML5 video.
                              </video>
                            <?php } ?>
                          </div>
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                      </div>
                    </div>
                  </div>
            </div>

        </div>
    </main>

@include('layouts.footer')