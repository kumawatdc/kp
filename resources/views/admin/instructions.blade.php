@include('layouts.left-side')
<script src="{{asset('/ckeditor/ckeditor.js')}}"> </script> 
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
          
            <h1 class="mt-4">Instructions</h1>
            <!-- <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Customer base</li>
            </ol> -->
            <div class="row">
            <div class="col-xl-12 col-md-12 mb-4">
                  <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                      <div class="row no-gutters align-items-center">
                        
                        <form id="page-form"  method="post" action="<?php echo url('/'); ?>/admin/update-instructions" enctype="multipart/form-data" role="form">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="id" value="<?php echo $pages->id; ?>">
                          <div class="form-group">
                            <label for="exampleInputAddress">Description</label>
                            <textarea name="text" id="editor" rows="10" class="form-control required ckeditor" placeholder="Description" cols="100" aria-required="true"><?php echo $pages->text ?></textarea>
                          </div>
                          
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                      </div>
                    </div>
                  </div>
            </div>

        </div>
    </main>
<script type="text/javascript">
    CKEDITOR.replace( 'editor', {
            height: 300,
            filebrowserUploadUrl: "<?php echo url('/'); ?>ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "<?php echo url('/'); ?>/ajaxfile.php?type=image"
        } );
  </script>
@include('layouts.footer')