@include('layouts.left-side')
<div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Statistics</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Statistics</li>
                        </ol>
                        <div class="row">
                        <div class="col-xl-6 col-md-6 mb-4">
                                  <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                      <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                          <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Number of Users</div>
                                          <div class="h5 mb-0 font-weight-bold text-gray-800"><?php if(isset($totalusers) && !empty($totalusers)) {
                                           echo $totalusers;
                                          } ?></div>
                                        </div>
                                        <div class="col-auto">
                                          <i class="fas fa-user-friends fa-2x text-gray-300"></i>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                        </div>
                        <div class="col-xl-6 col-md-6 mb-4">
                              <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                  <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">New Users Today</div>
                                      <div class="h5 mb-0 font-weight-bold text-gray-800">
                                        <?php if(isset($todayusers) && !empty($todayusers)) {
                                           echo $todayusers;
                                          } ?></div>
                                    </div>
                                    <div class="col-auto">
                                      <i class="fas fa-users fa-2x text-gray-300"></i>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                           
                        </div>
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header"><i class="fas fa-chart-area mr-1"></i>Area Chart Example</div>
                                    <div class="card-body"><canvas id="myAreaChart" width="100%" height="40"></canvas></div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header"><i class="fas fa-chart-bar mr-1"></i>Number of Users</div>
                                    <div class="card-body"><canvas id="myBarChart" width="100%" height="40"></canvas></div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header"><i class="fas fa-chart-bar mr-1"></i>Number of Download videos</div>
                                    <div class="card-body"><canvas id="myBarChartdownloadvideo" width="100%" height="40"></canvas></div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header"><i class="fas fa-chart-bar mr-1"></i>Number of Watch videos</div>
                                    <div class="card-body"><canvas id="myBarChartwatchvideo" width="100%" height="40"></canvas></div>
                                </div>
                            </div>
                            <?php $locationarray = '';
                                $lat = "";
                                $long = "";
                                // echo "<pre>";
                                // print_r($pages);
                                // echo "</pre>";
                                foreach ($pages as $keydr => $valuedr) {
                                  $username = (@ucfirst($valuedr->name)) ?: '';
                                  //if(isset($valuedr->latitude) && !empty($valuedr->latitude)){
                                    $lat      = (@$valuedr->lat) ?: '';
                                    $long     = (@$valuedr->long) ?: '';
                                    $locationarray .= "['".$username."',". $lat.",". $long.",".$keydr."],";
                                  }
                                  
                            ?>
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                <!--  <script src="https://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script> -->
                                
                                <div class="card-header"><i class="fas fa-chart-bar mr-1"></i>Users on map</div>
                                <div class="card-body">
                                    <div id="map"></div>
                                </div>
                                </div>
                            </div>

                        </div>
                        
                    </div>
                </main>
@include('layouts.footer')

<script type="text/javascript">
    Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Bar Chart Example
var ctx = document.getElementById("myBarChart");
var myLineChart = new Chart(ctx, {
  type: 'bar',
  data: {
    // labels: ["January", "February", "March", "April", "May", "June"],
    labels: <?php echo $josninmo; ?>,
    datasets: [{
      label: "Users",
      backgroundColor: "rgba(2,117,216,1)",
      borderColor: "rgba(2,117,216,1)",
      data: <?php echo $josnvalue; ?>,
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'month'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 6
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: <?php echo $totalusers; ?>,
          maxTicksLimit: 5
        },
        gridLines: {
          display: true
        }
      }],
    },
    legend: {
      display: false
    }
  }
});
</script>


<script type="text/javascript">
    Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Bar Chart Example
var ctx = document.getElementById("myBarChartdownloadvideo");
var myLineChart = new Chart(ctx, {
  type: 'bar',
  data: {
    // labels: ["January", "February", "March", "April", "May", "June"],
    labels: <?php echo $josndwnlvideo; ?>,
    datasets: [{
      label: "Users",
      backgroundColor: "rgba(2,117,216,1)",
      borderColor: "rgba(2,117,216,1)",
      data: <?php echo $josndwnlvideovalue; ?>,
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'month'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 6
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: <?php echo $totaldownvideo; ?>,
          maxTicksLimit: 5
        },
        gridLines: {
          display: true
        }
      }],
    },
    legend: {
      display: false
    }
  }
});
</script>

<script type="text/javascript">
    Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Bar Chart Example
var ctx = document.getElementById("myBarChartwatchvideo");
var myLineChart = new Chart(ctx, {
  type: 'bar',
  data: {
    // labels: ["January", "February", "March", "April", "May", "June"],
    labels: <?php echo $josnwatchvideo; ?>,
    datasets: [{
      label: "Users",
      backgroundColor: "rgba(2,117,216,1)",
      borderColor: "rgba(2,117,216,1)",
      data: <?php echo $josnwatchvideovalue; ?>,
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'month'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 6
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: <?php echo $totalwatchvideo; ?>,
          maxTicksLimit: 5
        },
        gridLines: {
          display: true
        }
      }],
    },
    legend: {
      display: false
    }
  }
});
</script>

<?php if(isset($lat) && !empty($lat)){ ?>
<script>

    function initMap() {
       var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: {lat: <?php echo $lat;?>, lng: <?php echo $long;?>}
       });

    setMarkers(map);
}
    var beaches = [<?php echo $locationarray; ?>];

    function setMarkers(map) {
      for (var i = 0; i < beaches.length; i++) {
          var beach = beaches[i];
          var marker = new google.maps.Marker({
            position: {lat: beach[1], lng: beach[2]},
            map: map,
            title: beach[0],
            zIndex: beach[3]
          });
        }
      }
</script>




<?php } ?>
<style>
#map{
    width:100%;
   height:600px;
}
</style>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCp1TxpXjXcqguGFxYKRXYmgg6JDA1ixEA&callback=initMap">
    </script>