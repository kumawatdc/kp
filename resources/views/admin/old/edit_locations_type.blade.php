@include('layouts.left-side')
<script src="{{asset('/ckeditor/ckeditor.js')}}"> </script> 
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
      <li class="breadcrumb-item">
      <a href="<?php echo url('/'); ?>/admin/">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">
      <a href="<?php echo url('/'); ?>/admin/manage-department-sub-menu">Locations-Type</a>
      </li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
        <i class="fas fa-users"></i>
        Edit Locations-Type</div>
        <div class="card-body">
        <form method="post" action="<?php echo url('/'); ?>/admin/save-locations-type" enctype="multipart/form-data" role="form" id="page-form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="<?php echo $department->id; ?>">
        <div class="form-group">
          <label for="exampleInputEmail1">Locations</label>
          <select name="locations_id" class="form-control required">
            <?php foreach ($pages as $key => $value) { ?>
              <option value="<?php echo $value->id; ?>" <?php if($value->id == $department->locations_id){ echo "Selected"; }?>><?php echo $value->title; ?></option>
            <?php
            } ?>
          </select>
        </div>
        <a id="Country" href="JavaScript:void(0);">Show/Hide Country</a><br>
        <div class="form-group Countrys">
          <label for="exampleInputEmail1">English Country</label>
          <input type="text" class="form-control required" id="en_country" aria-describedby="emailHelp" value="<?php echo $department->en_country; ?>" name="en_country">
        </div>
        <div class="form-group Countrys">
          <label for="exampleInputEmail1">Portuguese Country</label>
          <input type="text" class="form-control required" id="pt_country" aria-describedby="emailHelp" value="<?php echo $department->pt_country; ?>" name="pt_country">
        </div>
        <div class="form-group Countrys">
          <label for="exampleInputEmail1">Polish Country</label>
          <input type="text" class="form-control required" id="ps_country" aria-describedby="emailHelp" value="<?php echo $department->ps_country; ?>" name="ps_country">
        </div>
        <div class="form-group Countrys">
          <label for="exampleInputEmail1">Spanish Country</label>
          <input type="text" class="form-control required" id="es_country" aria-describedby="emailHelp" value="<?php echo $department->es_country; ?>" name="es_country">
        </div>
        <a id="Title" href="JavaScript:void(0);">Show/Hide Title</a><br>
        <div class="form-group Titles">
          <label for="exampleInputEmail1">English Title</label>
          <input type="text" class="form-control required" id="en_title" aria-describedby="emailHelp" value="<?php echo $department->en_title; ?>" name="en_title">
        </div>
        <div class="form-group Titles">
          <label for="exampleInputEmail1">Portuguese Title</label>
          <input type="text" class="form-control required" id="pt_title" aria-describedby="emailHelp" value="<?php echo $department->pt_title; ?>" name="pt_title">
        </div>
        <div class="form-group Titles">
          <label for="exampleInputEmail1">Polish Title</label>
          <input type="text" class="form-control required" id="ps_title" aria-describedby="emailHelp" value="<?php echo $department->ps_title; ?>" name="ps_title">
        </div>
        <div class="form-group Titles">
          <label for="exampleInputEmail1">Spanish Title</label>
          <input type="text" class="form-control required" id="es_title" aria-describedby="emailHelp" value="<?php echo $department->es_title; ?>" name="es_title">
        </div>
        <a id="Telephone" href="JavaScript:void(0);">Show/Hide Telephone</a><br>
        <div class="form-group Telephones">
          <label for="exampleInputEmail1">English Telephone</label>
          <input type="text" class="form-control required" id="en_telephone" aria-describedby="emailHelp" value="<?php echo $department->en_telephone; ?>" name="en_telephone">
        </div>
        <div class="form-group Telephones">
          <label for="exampleInputEmail1">Portuguese Telephone</label>
          <input type="text" class="form-control required" id="pt_telephone" aria-describedby="emailHelp" value="<?php echo $department->pt_telephone; ?>" name="pt_telephone">
        </div>
        <div class="form-group Telephones">
          <label for="exampleInputEmail1">Polish Telephone</label>
          <input type="text" class="form-control required" id="ps_telephone" aria-describedby="emailHelp" value="<?php echo $department->ps_telephone; ?>" name="ps_telephone">
        </div>
        <div class="form-group Telephones">
          <label for="exampleInputEmail1">Spanish Telephone</label>
          <input type="text" class="form-control required" id="es_telephone" aria-describedby="emailHelp" value="<?php echo $department->es_telephone; ?>" name="es_telephone">
        </div>
        <a id="walkinclinic" href="JavaScript:void(0);">Show/Hide Walk-in clinic</a><br>
        <div class="form-group walkinclinics">
          <label for="exampleInputEmail1">English Walk-in clinic</label>
          <input type="text" class="form-control required" id="en_walkinclinic" aria-describedby="emailHelp" value="<?php echo $department->en_walkinclinic; ?>" name="en_walkinclinic">
        </div>
        <div class="form-group walkinclinics">
          <label for="exampleInputEmail1">Portuguese Walk-in clinic</label>
          <input type="text" class="form-control required" id="pt_walkinclinic" aria-describedby="emailHelp" value="<?php echo $department->pt_walkinclinic; ?>" name="pt_walkinclinic">
        </div>
        <div class="form-group walkinclinics">
          <label for="exampleInputEmail1">Polish Walk-in clinic</label>
          <input type="text" class="form-control required" id="ps_walkinclinic" aria-describedby="emailHelp" value="<?php echo $department->ps_walkinclinic; ?>" name="ps_walkinclinic">
        </div>
        <div class="form-group walkinclinics">
          <label for="exampleInputEmail1">Spanish Walk-in clinic</label>
          <input type="text" class="form-control required" id="es_walkinclinic" aria-describedby="emailHelp" value="<?php echo $department->es_walkinclinic; ?>" name="es_walkinclinic">
        </div>
        <a id="Time" href="JavaScript:void(0);">Show/Hide Time</a><br>
        <div class="form-group Times">
          <label for="exampleInputEmail1">English Time</label>
          <input type="text" class="form-control required" id="en_time" aria-describedby="emailHelp" value="<?php echo $department->en_time; ?>" name="en_time">
        </div>
        <div class="form-group Times">
          <label for="exampleInputEmail1">Portuguese Time</label>
          <input type="text" class="form-control required" id="pt_time" aria-describedby="emailHelp" value="<?php echo $department->ps_time; ?>" name="pt_time">
        </div>
        <div class="form-group Times">
          <label for="exampleInputEmail1">Polish Time</label>
          <input type="text" class="form-control required" id="ps_time" aria-describedby="emailHelp" value="<?php echo $department->ps_time; ?>" name="ps_time">
        </div>
        <div class="form-group Times">
          <label for="exampleInputEmail1">Spanish Time</label>
          <input type="text" class="form-control required" id="es_time" aria-describedby="emailHelp" value="<?php echo $department->es_time; ?>" name="es_time">
        </div>
        <a id="how_much" href="JavaScript:void(0);">Show/Hide How Much</a><br>
        <div class="form-group how_muchs">
          <label for="exampleInputEmail1">English How Much</label>
          <input type="text" class="form-control required" id="en_how_much" aria-describedby="emailHelp" value="<?php echo $department->en_how_much; ?>" name="en_how_much">
        </div>
        <div class="form-group how_muchs">
          <label for="exampleInputEmail1">Portuguese How Much</label>
          <input type="text" class="form-control required" id="pt_how_much" aria-describedby="emailHelp" value="<?php echo $department->pt_how_much; ?>" name="pt_how_much">
        </div>
        <div class="form-group how_muchs">
          <label for="exampleInputEmail1">Polish How Much</label>
          <input type="text" class="form-control required" id="ps_how_much" aria-describedby="emailHelp" value="<?php echo $department->ps_how_much; ?>" name="ps_how_much">
        </div>
        <div class="form-group how_muchs">
          <label for="exampleInputEmail1">Spanish How Much</label>
          <input type="text" class="form-control required" id="es_how_much" aria-describedby="emailHelp" value="<?php echo $department->es_how_much; ?>" name="es_how_much">
        </div>

        <a id="Description" href="JavaScript:void(0);">Show/Hide Descriptions</a><br>
        <div class="form-group Descriptions">
          <label for="exampleInputPassword1">English Descriptions</label>
          <textarea name="en_description" id="editor" class="form-control required ckeditor" rows="7" cols="80"><?php echo $department->en_description; ?></textarea>
        </div>
        <div class="form-group Descriptions">
          <label for="exampleInputPassword1">Portuguese Descriptions</label>
          <textarea name="pt_description" id="pt_editor" class="form-control required ckeditor" rows="7" cols="80"><?php echo $department->pt_description; ?></textarea>
        </div>
        <div class="form-group Descriptions">
          <label for="exampleInputPassword1">Polish Descriptions</label>
          <textarea name="ps_description" id="ps_editor" class="form-control required ckeditor" rows="7" cols="80"><?php echo $department->ps_description; ?></textarea>
        </div>
        <div class="form-group Descriptions">
          <label for="exampleInputPassword1">Spanish Descriptions</label>
          <textarea name="es_description" id="es_editor" class="form-control required ckeditor" rows="7" cols="80"><?php echo $department->es_description; ?></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      $( "#Country" ).click(function() {
        $( ".Countrys" ).toggle();
      });
      $( "#Title" ).click(function() {
        $( ".Titles" ).toggle();
      });
      $( "#Telephone" ).click(function() {
        $( ".Telephones" ).toggle();
      });
      $( "#walkinclinic" ).click(function() {
        $( ".walkinclinics" ).toggle();
      });
      $( "#Time" ).click(function() {
        $( ".Times" ).toggle();
      });
      $( "#how_much" ).click(function() {
        $( ".how_muchs" ).toggle();
      });
      $( "#Description" ).click(function() {
        $( ".Descriptions" ).toggle();
      });

      CKEDITOR.replace( 'editor', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
      CKEDITOR.replace( 'pt_editor', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
      CKEDITOR.replace( 'ps_editor', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
      CKEDITOR.replace( 'es_editor', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
  </script>
@include('layouts.footer')