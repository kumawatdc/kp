@include('layouts.left-side')
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
      <li class="breadcrumb-item">
      <a href="<?php echo url('/'); ?>/admin/">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">
      <a href="<?php echo url('/'); ?>/admin/manage-package-list">Manage Package</a>
      </li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
        <i class="fas fa-users"></i>
        Add Package</div>
        <div class="card-body">
        <form method="post" action="<?php echo url('/'); ?>/admin/new-package" enctype="multipart/form-data" role="form" id="page-form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
          <label for="exampleInputEmail1">Title</label>
          <input type="text" class="form-control required" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Title" name="title">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Address</label>
          <textarea class="form-control required" id="address" aria-describedby="emailHelp" placeholder="Address" name="address"></textarea>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">City</label>
          <input type="text" class="form-control required" id="city" aria-describedby="emailHelp" placeholder="City" name="city">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Zip Code</label>
          <input type="text" class="form-control required" id="zip_code" aria-describedby="emailHelp" placeholder="Zip Code" name="zip_code">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Business Houres</label>
          <select class="form-control required" name="business_houres">
            <option value="">Select Business Houres</option>
            <option value="Mon-Sat: 7AM to 9PM">Mon-Sat: 7AM to 9PM</option>
            <option value="Sun: 9AM to 6PM">Sun: 9AM to 6PM</option>
          </select>
          
        </div>
        <div class="form-group">
          <label for="exampleFormControlFile1">Image</label>
          <input type="file" name="images" class="form-control-file required" id="exampleFormControlFile1">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        </div>
      </div>
    </div>
@include('layouts.footer')