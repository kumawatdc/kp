@include('layouts.left-side')
<script src="{{asset('/ckeditor/ckeditor.js')}}"> </script> 
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
      <li class="breadcrumb-item">
      <a href="<?php echo url('/'); ?>/admin/">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">
      <a href="<?php echo url('/'); ?>/admin/manage-department-menu">Symptoms</a>
      </li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
        <i class="fas fa-users"></i>
        Edit Symptoms</div>
        <div class="card-body">
        <form method="post" action="<?php echo url('/'); ?>/admin/save-symptoms" enctype="multipart/form-data" role="form" id="page-form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="<?php echo $pages->id; ?>">
        <input type="hidden" name="old_image" value="<?php echo $pages->image; ?>">
        <div class="form-group">
          <label for="exampleInputEmail1">English Title</label>
          <input type="text" class="form-control required" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php echo $pages->title; ?>" name="title">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Portuguese Title</label>
          <input type="text" class="form-control required" id="pt_title" aria-describedby="emailHelp" value="<?php echo $pages->pt_title; ?>" name="pt_title">
        </div>
         <div class="form-group">
          <label for="exampleInputEmail1">Polish Title</label>
          <input type="text" class="form-control required" id="ps_title" aria-describedby="emailHelp" value="<?php echo $pages->ps_title; ?>" name="ps_title">
        </div>
         <div class="form-group">
          <label for="exampleInputEmail1">Spanish Title</label>
          <input type="text" class="form-control required" id="es_title" aria-describedby="emailHelp" value="<?php echo $pages->es_title; ?>" name="es_title">
        </div>
       <!--  <div class="form-group">
          <label for="exampleFormControlFile1">Image</label>
          <input type="file" name="images" class="form-control-file" id="exampleFormControlFile1">
          <?php if(isset($pages->image) && !empty($pages->image)) { ?>
            <br>
            <img width="100" height="100" class="rounded-circle" src="<?php echo url('/'); ?>/gallery/<?php echo $pages->image ?>">
          <?php
          } ?>
        </div> -->
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        </div>
      </div>
    </div>
     <script type="text/javascript">
    CKEDITOR.replace( 'editor', {
            height: 300,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
  </script>
@include('layouts.footer')