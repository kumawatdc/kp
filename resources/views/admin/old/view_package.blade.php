@include('layouts.left-side')
<script src="{{asset('/ckeditor/ckeditor.js')}}"> </script> 
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
      <li class="breadcrumb-item">
      <a href="<?php echo url('/'); ?>/admin/">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">
      <a href="<?php echo url('/'); ?>/admin/manage-package-list">Manage Package</a>
      </li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
        <i class="fas fa-users"></i>
        View Package</div>
        <div class="card-body">
          <div class="row">
            <div class="col-12">
             <div class="card">
                    <div class="card-block">
                      <div class="row">
                        <div class="col-md-2 col-sm-4 text-center">
                        <?php if(!empty($pages->image)){ ?> 
                          <img src="<?php echo url('/'); ?>/gallery/<?php echo $pages->image ?>" class="img-fluid" width="150">
                          <?php } else{ ?>
                          <img src="<?php echo url('/'); ?>/gallery/userdefault.png" alt="" class="img-fluid" width="150">
                          <?php
                        }?>

                        </div>
                        <div class="col-md-4 col-sm-8">
                          <h2 class="card-title">Title: <?php if(isset($pages->title) && !empty($pages->title)) { 
                          echo $pages->title; } ?></h2>
                         
                          <p class="card-text"><strong>Address: </strong> <?php echo $pages->address; ?> </p>
                          <p class="card-text"><strong>City: </strong> <?php echo $pages->city; ?> </p>
                          <p class="card-text"><strong>Zip Code: </strong> <?php echo $pages->zip_code; ?> </p>
                          <p class="card-text"><strong>Time: </strong> <?php echo $pages->time; ?> </p>
                          <p><strong>Delivered Status: </strong>
                          <?php if($pages->status == 1) {  ?>
                          <span class="badge bg-danger">Pending</span>
                          <?php } else{ ?>
                          <span class="badge bg-success">Delivered</span>
                          <?php }?>
                          </p>
                          <p><strong>Active Status: </strong>
                          <?php if($pages->active_status == 1) {  ?>
                          <span class="badge bg-success">Active</span>
                          <?php } else{ ?>
                          <span class="badge bg-danger">Pending</span>
                          <?php } ?>
                          </p>
                          <p class="card-text"><strong>Created Date: </strong> 
                          <?php $date = $pages->created_at; echo date('F d,Y',strtotime($date)); ?>
                          </p>
                        </div> 

                        <div class="col-md-2 col-sm-4 text-center">
                        <?php if(!empty($pages->userimage)){ ?> 
                          <img src="<?php echo url('/'); ?>/users/<?php echo $pages->userimage ?>" class="img-fluid" width="150">
                          <?php } else{ ?>
                          <img src="<?php echo url('/'); ?>/users/userdefault.png" alt="" class="img-fluid" width="150">
                          <?php
                        }?>

                        </div>
                        <div class="col-md-4 col-sm-8">
                          <h2 class="card-title">Name: <?php if(isset($pages->username) && !empty($pages->username)) { 
                          echo $pages->username; } ?></h2>
                         
                          <p class="card-text"><strong>City: </strong> <?php echo $pages->usercity; ?> </p>
                          <p class="card-text"><strong>Email: </strong> <?php echo $pages->useremail; ?> </p>
                          <p class="card-text"><strong>Phone: </strong> <?php echo $pages->userphone; ?> </p>
                          
                        </div> 
                         
                        
                      </div>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
     <script type="text/javascript">
    CKEDITOR.replace( 'editor', {
            height: 300,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
  </script>
@include('layouts.footer')