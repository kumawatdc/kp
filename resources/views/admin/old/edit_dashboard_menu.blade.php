@include('layouts.left-side')
<script src="{{asset('/ckeditor/ckeditor.js')}}"> </script> 
      <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="<?php echo url('/'); ?>/admin/">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">
            <a href="<?php echo url('/'); ?>/admin/manage-menu">Manage Menu</a>
          </li>
        </ol>
        
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-users"></i> Edit Menu
          </div>
          <div class="card-body">
            <form method="post" action="<?php echo url('/'); ?>/admin/save-dashboard-menu" enctype="multipart/form-data" role="form" id="page-form">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="id" value="<?php echo $pages->id; ?>">
              <input type="hidden" name="old_image" value="<?php echo $pages->image ?>">
              <?php if($pages->title != "Gallery") { ?>
                <?php if($pages->title == "Departments") { ?>
                  <div class="form-group">
                  <label for="exampleInputEmail1">Title</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Title" name="title" value="<?php echo $pages->title; ?>" readonly>
                </div>
                <?php }else{ ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">Title</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Title" name="title" value="<?php echo $pages->title; ?>">
                </div>
                <?php } ?>
                <div class="form-group">
                  <label for="exampleInputPassword1">Description</label>
                  <textarea name="description" id="editor" class="form-control ckeditor" rows="7" cols="80"><?php echo $pages->text; ?></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleFormControlFile1">Image file input</label>
                  <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                  <?php if(isset($pages->image) && !empty($pages->image)) { ?>
                    <br>
                    <img width="100" height="100" class="rounded-circle" src="<?php echo url('/'); ?>/gallery/<?php echo $pages->image ?>">
                  <?php
                  } ?>
                </div>
              <?php } ?>

              <?php if($pages->title == "Gallery") { ?>
                <input type="hidden" name="description" value="<?php echo $pages->text; ?>">
                <div class="form-group">
                  <label for="exampleInputEmail1">Title</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Title" name="title" value="<?php echo $pages->title; ?>" readonly>
                </div>
                <div class="form-group">
                  <label for="exampleFormControlFile1">Image file input</label>
                  <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                  <?php if(isset($pages->image) && !empty($pages->image)) { ?>
                    <br>
                    <img width="100" height="100" class="rounded-circle" src="<?php echo url('/'); ?>/gallery/<?php echo $pages->image ?>">
                  <?php
                  } ?>
                </div>
               <!--  <div class="form-group">
                  <label for="exampleFormControlFile1">Image file input</label>
                  <input type="file" name="image[]" class="form-control-file" id="exampleFormControlFile1">
                </div> -->
                <?php foreach ($multimgdb as $key => $value) { ?>
                    <label for="multiimage">*Image <?php echo $key+1; ?></label>
                    <img width="100" height="100" class="rounded-circle" src="<?php echo url('/'); ?>/gallery/<?php echo $value->images; ?>">
                    <a href="<?php echo url('/'); ?>/admin/remove-dashboard-gallery/<?php echo $value->id; ?>">Remove</a>
                <?php  } ?> 

                <div class="form-group">
                  <div class="input_fields_wrap">
                    <button class="add_field_button">Add More Images</button>
                  </div>
                </div>

            <?php } ?>

              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
      <script type="text/javascript">
    CKEDITOR.replace( 'editor', {
            height: 300,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
  </script>
<script type="text/javascript">
  
  $(document).ready(function() {
  var max_fields      = 20; //maximum input boxes allowed
  var wrapper       = $(".input_fields_wrap"); //Fields wrapper
  var add_button      = $(".add_field_button"); //Add button ID
  
  var x = 1; //initlal text box count
  $(add_button).click(function(e){ //on add input button click
    e.preventDefault();
    if(x < max_fields){ //max input box allowed
      x++; //text box increment
      $(wrapper).append('<div class="form-group"><label for="multiimage">*Image</label><input type="file" name="multiimage[]" class="form-control required" id="exampleFormControlFile1" /><br><a href="#" class="remove_field">Remove</a></div><br>'); //add input box
    }
  });
  
  $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
    e.preventDefault(); $(this).parent('div').remove(); x--;
  })
});

</script>
<style type="text/css">
  .remove_field{
    color: #FFF;
    background-color: #bd2130;
    border-color: #b21f2d;
    border: 2px solid transparent;
    padding: 0.375rem 0.75rem;
    float: right;
  }
</style>
@include('layouts.footer')