@include('layouts.left-side')
<script src="{{asset('/ckeditor/ckeditor.js')}}"> </script> 
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
      <li class="breadcrumb-item">
      <a href="<?php echo url('/'); ?>/admin/">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">
      <a href="<?php echo url('/'); ?>/admin/manage-users">Manage Users</a>
      </li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
        <i class="fas fa-users"></i>
        View Users</div>
        <div class="card-body">
          <div class="row">
            <div class="col-12">
             <div class="card">
                    <div class="card-block">
                      <div class="row">
                        <div class="col-md-4 col-sm-4 text-center">
                        <?php if(!empty($pages->image)){ ?> 
                          <img src="<?php echo url('/'); ?>/users/<?php echo $pages->image ?>" class="img-fluid" width="150">
                          <?php } else{ ?>
                          <img src="<?php echo url('/'); ?>/users/userdefault.png" alt="" class="img-fluid" width="150">
                          <?php
                        }?>

                        </div>
                        <div class="col-md-8 col-sm-8">
                          <h2 class="card-title">Name: <?php if(isset($pages->name) && !empty($pages->name)) { 
                          echo $pages->name; } ?></h2>
                          <p class="card-text"><strong>Email: </strong> <?php echo $pages->email; ?> </p>
                          <p class="card-text"><strong>Address: </strong> <?php echo $pages->address; ?> </p>
                          <p class="card-text"><strong>City: </strong> <?php echo $pages->city; ?> </p>
                          <p class="card-text"><strong>Mobile: </strong> <?php echo $pages->phone; ?> </p>
                          <p><strong>Status: </strong>
                          <?php if($pages->status == 1) {  ?>
                          <span class="badge bg-success">Approved</span>
                          <?php } else{?>
                          <span class="badge bg-danger">Pending</span>
                          <?php
                          }?>
                          </p>
                          <p class="card-text"><strong>Created Date: </strong> 
                          <?php $date = $pages->created_at; echo date('F d,Y',strtotime($date)); ?>
                          </p>
                        </div>              

                      </div>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
     <script type="text/javascript">
    CKEDITOR.replace( 'editor', {
            height: 300,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
  </script>
@include('layouts.footer')