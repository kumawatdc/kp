@include('layouts.left-side')
<script src="{{asset('/ckeditor/ckeditor.js')}}"> </script> 
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
      <li class="breadcrumb-item">
      <a href="<?php echo url('/'); ?>/admin/">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">
      <a href="<?php echo url('/'); ?>/admin/manage-department-menu">Termination & Abortion Info</a>
      </li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
        <i class="fas fa-users"></i>
        Termination & Abortion Info</div>
        <div class="card-body">
        <form method="post" action="<?php echo url('/'); ?>/admin/save-termination-abortion" enctype="multipart/form-data" role="form" id="page-form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="<?php echo $questions->id; ?>">
        <div class="form-group">
          <label for="exampleInputEmail1">Questions</label>
          <input type="text" class="form-control required" id="title" aria-describedby="emailHelp" value="<?php echo $questions->title; ?>" name="title">
        </div>
        <?php $i = '0';
        foreach ($options as $key => $value) { $i++; ?>
          <div class="form-group">
            <label for="exampleInputEmail1">Questions Options <?php echo $i; ?></label>
            <input type="text" class="form-control required" id="oprion_<?php echo $key; ?>" aria-describedby="emailHelp" value="<?php echo $value->title; ?>" name="oprion[<?php echo $value->id; ?>]">
          </div>
          <?php
        }?>
        <div class="form-group">
          <label for="exampleInputEmail1">Description</label>
          <textarea name="description" id="description" class="form-control required ckeditor" rows="7" cols="80"><?php echo $description->description; ?></textarea>
        </div>
        <!-- <div class="form-group">
          <label for="exampleFormControlFile1">Image</label>
          <input type="file" name="images" class="form-control-file required" id="exampleFormControlFile1">
        </div> -->
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      CKEDITOR.replace( 'description', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
    </script>
@include('layouts.footer')