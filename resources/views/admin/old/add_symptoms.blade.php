@include('layouts.left-side')
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
      <li class="breadcrumb-item">
      <a href="<?php echo url('/'); ?>/admin/">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">
      <a href="<?php echo url('/'); ?>/admin/manage-department-menu">Symptoms</a>
      </li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
        <i class="fas fa-users"></i>
        Add Symptoms</div>
        <div class="card-body">
        <form method="post" action="<?php echo url('/'); ?>/admin/new-symptoms" enctype="multipart/form-data" role="form" id="page-form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
          <label for="exampleInputEmail1">English Title</label>
          <input type="text" class="form-control required" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="English Title" name="title">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Portuguese Title</label>
          <input type="text" class="form-control required" id="pt_title" aria-describedby="emailHelp" placeholder="Portuguese Title" name="pt_title">
        </div>
         <div class="form-group">
          <label for="exampleInputEmail1">Polish Title</label>
          <input type="text" class="form-control required" id="ps_title" aria-describedby="emailHelp" placeholder="Polish Title" name="ps_title">
        </div>
         <div class="form-group">
          <label for="exampleInputEmail1">Spanish Title</label>
          <input type="text" class="form-control required" id="es_title" aria-describedby="emailHelp" placeholder="Spanish Title" name="es_title">
        </div>
        <!-- <div class="form-group">
          <label for="exampleFormControlFile1">Image</label>
          <input type="file" name="images" class="form-control-file required" id="exampleFormControlFile1">
        </div> -->
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        </div>
      </div>
    </div>
@include('layouts.footer')