@include('layouts.left-side')
<script src="{{asset('/ckeditor/ckeditor.js')}}"> </script> 
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
      <li class="breadcrumb-item">
      <a href="<?php echo url('/'); ?>/admin/">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">
      <a href="<?php echo url('/'); ?>/admin/manage-dealers">Manage Dealers</a>
      </li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
        <i class="fas fa-users"></i>
        View Dealers</div>
        <div class="card-body">
          <div class="row">
            <div class="col-12">
             <div class="card">
                    <div class="card-block">
                      <div class="row">
                        <div class="col-md-2 col-sm-4 text-center">
                        <?php if(!empty($pages->image)){ ?> 
                          <img src="<?php echo url('/'); ?>/users/<?php echo $pages->image ?>" class="img-fluid" width="150">
                          <?php } else{ ?>
                          <img src="<?php echo url('/'); ?>/users/userdefault.png" alt="" class="img-fluid" width="150">
                          <?php
                        }?>

                        </div>
                        <div class="col-md-6 col-sm-8">
                          <h2 class="card-title">Name: <?php if(isset($pages->name) && !empty($pages->name)) { 
                          echo $pages->name; } ?></h2>
                          <?php if (isset($pages->email) && !empty($pages->email)) { ?>
                            <p class="card-text"><strong>Email: </strong> <?php echo $pages->email; ?> </p>
                          <?php } ?>
                          <?php if (isset($pages->userbusinessname) && !empty($pages->userbusinessname)) { ?>
                            <p class="card-text"><strong>Business Name: </strong> <?php echo $pages->business_name; ?> </p>
                          <?php } ?>
                          <p class="card-text"><strong>Address: </strong> <?php echo $pages->address; ?> </p>
                          <p class="card-text"><strong>City: </strong> <?php echo $pages->city; ?> </p>
                          <p class="card-text"><strong>Mobile: </strong> <?php echo $pages->phone; ?> </p>
                          <p><strong>Status: </strong>
                          <?php if($pages->role == 0) {  ?>
                          <span class="badge bg-danger">Pending</span>
                          <?php } else { ?>
                          <span class="badge bg-success">Approved</span>
                          <?php }?>
                          </p>
                          <p class="card-text"><strong>Created Date: </strong> 
                          <?php $date = $pages->created_at; echo date('F d,Y',strtotime($date)); ?>
                          </p>
                        </div> 
                        <div class="col-md-2 col-sm-4 text-center">
                        <?php if(!empty($pages->driver_license)){ ?> 
                          <img src="<?php echo url('/'); ?>/users/<?php echo $pages->driver_license; ?>" class="img-fluid" width="150">
                          <?php } else{ ?>
                          <img src="<?php echo url('/'); ?>/users/default.png" alt="" class="img-fluid" width="150">
                          <?php
                        }?>
                        <strong>Driver License</strong>
                        </div>
                        <div class="col-md-2 col-sm-4 text-center">
                        <?php if(!empty($pages->insurance)){ ?> 
                          <img src="<?php echo url('/'); ?>/users/<?php echo $pages->insurance; ?>" class="img-fluid" width="150">
                          <?php } else{ ?>
                          <img src="<?php echo url('/'); ?>/users/default.png" alt="" class="img-fluid" width="150">
                          <?php
                        }?>
                        <strong>Insurance</strong>
                        </div> 
                        <div class="col-md-2 col-sm-4 text-center">
                        <?php if(!empty($pages->live_scan)){ ?> 
                          <img src="<?php echo url('/'); ?>/users/<?php echo $pages->live_scan; ?>" class="img-fluid" width="150">
                          <?php } else{ ?>
                          <img src="<?php echo url('/'); ?>/users/default.png" alt="" class="img-fluid" width="150">
                          <?php
                        }?>
                        <strong>Live Scan</strong>
                        </div>
                        <div class="col-md-2 col-sm-4 text-center">
                        <?php if(!empty($pages->surety_bond)){ ?> 
                          <img src="<?php echo url('/'); ?>/users/<?php echo $pages->surety_bond; ?>" class="img-fluid" width="150">
                          <?php } else{ ?>
                          <img src="<?php echo url('/'); ?>/users/default.png" alt="" class="img-fluid" width="150">
                          <?php
                        }?>
                        <strong>Surety Bond</strong>
                        </div> 
                        <?php if($pages->role == 0) {  ?>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                              Become a Dealers
                            </button>
                        <div class="col-md-4 col-sm-4 text-center">

                          <!-- The Modal -->
                          <div class="modal" id="myModal">
                            <div class="modal-dialog">
                              <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                  <h4 class="modal-title">Create Password</h4>
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- Modal body -->
                                <div class="modal-body">
                                  <div class="card-body text-left">
                                  <form method="post" action="<?php echo url('/'); ?>/admin/save-dealers" enctype="multipart/form-data" role="form" id="page-form">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="id" value="<?php echo $pages->id; ?>">
                                      <div class="form-group">
                                        <label for="exampleInputEmail1">Password</label>
                                        <input type="text" class="form-control required" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Password" name="password">
                                      </div>
                                      
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                  </form>
                                </div>
                                </div>

                                <!-- Modal footer -->
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>

                              </div>
                            </div>
                          </div>

                        </div>
                      <?php } ?>
                      </div>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
     <script type="text/javascript">
    CKEDITOR.replace( 'editor', {
            height: 300,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
  </script>
@include('layouts.footer')