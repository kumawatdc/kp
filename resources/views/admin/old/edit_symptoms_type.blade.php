@include('layouts.left-side')
<script src="{{asset('/ckeditor/ckeditor.js')}}"> </script> 
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
      <li class="breadcrumb-item">
      <a href="<?php echo url('/'); ?>/admin/">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">
      <a href="<?php echo url('/'); ?>/admin/manage-department-sub-menu">Symptoms-Type</a>
      </li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
        <i class="fas fa-users"></i>
        Edit Symptoms-Type</div>
        <div class="card-body">
        <form method="post" action="<?php echo url('/'); ?>/admin/save-symptoms-type" enctype="multipart/form-data" role="form" id="page-form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="<?php echo $department->id; ?>">
        <div class="form-group">
          <label for="exampleInputEmail1">Department Menu</label>
          <select name="symptoms_id" class="form-control required">
            <?php foreach ($pages as $key => $value) { ?>
              <option value="<?php echo $value->id; ?>" <?php if($value->id == $department->symptoms_id){ echo "Selected"; }?>><?php echo $value->title; ?></option>
            <?php
            } ?>
          </select>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">English Title</label>
          <input type="text" class="form-control required" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php echo $department->title; ?>" name="title">
        </div>
         <div class="form-group">
          <label for="exampleInputEmail1">Portuguese Title</label>
          <input type="text" class="form-control required" id="pt_title" aria-describedby="emailHelp" value="<?php echo $department->pt_title; ?>" name="pt_title">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Polish Title</label>
          <input type="text" class="form-control required" id="ps_title" aria-describedby="emailHelp" value="<?php echo $department->ps_title; ?>" name="ps_title">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Spanish Title</label>
          <input type="text" class="form-control required" id="es_title" aria-describedby="emailHelp" value="<?php echo $department->es_title; ?>" name="es_title">
        </div>
        <a id="Conditions" href="JavaScript:void(0);">Show/Hide Conditions-Details</a><br>
        <div class="form-group EnglishC">
          <label for="exampleInputPassword1">English Conditions-Details</label>
          <textarea name="conditions" id="editor" class="form-control required ckeditor" rows="7" cols="80"><?php echo $department->conditions; ?></textarea>
        </div>
        <div class="form-group EnglishC">
          <label for="exampleInputPassword1">Portuguese Conditions-Details</label>
          <textarea name="pt_conditions" id="pt_editor" class="form-control required ckeditor" rows="7" cols="80"><?php echo $department->pt_conditions; ?></textarea>
        </div>
        <div class="form-group EnglishC">
          <label for="exampleInputPassword1">Polish Conditions-Details</label>
          <textarea name="ps_conditions" id="ps_editor" class="form-control required ckeditor" rows="7" cols="80"><?php echo $department->ps_conditions; ?></textarea>
        </div>
        <div class="form-group EnglishC">
          <label for="exampleInputPassword1">Spanish Conditions-Details</label>
          <textarea name="es_conditions" id="es_editor" class="form-control required ckeditor" rows="7" cols="80"><?php echo $department->es_conditions; ?></textarea>
        </div>
        <a id="Treatment" href="JavaScript:void(0);">Show/Hide Treatment-Options</a><br>
        <div class="form-group Englisht">
          <label for="exampleInputPassword1">English Treatment-Options</label>
          <textarea name="treatment" id="editor1" class="form-control required ckeditor" rows="7" cols="80"><?php echo $department->treatment; ?></textarea>
        </div>
        <div class="form-group Englisht">
          <label for="exampleInputPassword1">Portuguese Treatment-Options</label>
          <textarea name="pt_treatment" id="pt_treatment" class="form-control required ckeditor" rows="7" cols="80"><?php echo $department->pt_treatment; ?></textarea>
        </div>
        <div class="form-group Englisht">
          <label for="exampleInputPassword1">Polish Treatment-Options</label>
          <textarea name="ps_treatment" id="ps_treatment" class="form-control required ckeditor" rows="7" cols="80"><?php echo $department->ps_treatment; ?></textarea>
        </div>
        <div class="form-group Englisht">
          <label for="exampleInputPassword1">Spanish Treatment-Options</label>
          <textarea name="es_treatment" id="es_treatment" class="form-control required ckeditor" rows="7" cols="80"><?php echo $department->es_treatment; ?></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        </div>
      </div>
    </div>
   <script type="text/javascript">
    $( "#Conditions" ).click(function() {
        $( ".EnglishC" ).toggle();
      });
      $( "#Treatment" ).click(function() {
        $( ".Englisht" ).toggle();
      });

      CKEDITOR.replace( 'editor', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
      CKEDITOR.replace( 'pt_editor', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
      CKEDITOR.replace( 'ps_editor', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
      CKEDITOR.replace( 'es_editor', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );

      CKEDITOR.replace( 'editor1', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
      CKEDITOR.replace( 'pt_treatment', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
      CKEDITOR.replace( 'ps_treatment', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
      CKEDITOR.replace( 'es_treatment', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
  </script>
@include('layouts.footer')