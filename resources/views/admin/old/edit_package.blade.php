@include('layouts.left-side')
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
      <li class="breadcrumb-item">
      <a href="<?php echo url('/'); ?>/admin/">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">
      <a href="<?php echo url('/'); ?>/admin/manage-package-list">Manage Package</a>
      </li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
        <i class="fas fa-users"></i>
        Edit Package</div>
        <div class="card-body">
        <form method="post" action="<?php echo url('/'); ?>/admin/save-package" enctype="multipart/form-data" role="form" id="page-form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="<?php echo $pages->id; ?>">
        <div class="form-group">
          <label for="exampleInputEmail1">Title</label>
          <input type="text" class="form-control required" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Title" value="<?php echo $pages->title; ?>" name="title">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Address</label>
          <textarea class="form-control required" id="address" aria-describedby="emailHelp" placeholder="Address" name="address"><?php echo $pages->address; ?></textarea>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">City</label>
          <input type="text" class="form-control required" id="city" aria-describedby="emailHelp" placeholder="City" name="city" value="<?php echo $pages->city; ?>">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Zip Code</label>
          <input type="text" class="form-control required" id="zip_code" aria-describedby="emailHelp" placeholder="Zip Code" name="zip_code" value="<?php echo $pages->zip_code; ?>">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Business Houres</label>
          <select class="form-control required" name="business_houres">
            <option value="">Select Business Houres</option>
            <option value="Mon-Sat: 7AM to 9PM" <?php if($pages->time == "Mon-Sat: 7AM to 9PM") { echo "selected"; } ?>>Mon-Sat: 7AM to 9PM</option>
            <option value="Sun: 9AM to 6PM" <?php if($pages->time == "Sun: 9AM to 6PM") { echo "selected"; } ?>>Sun: 9AM to 6PM</option>
          </select>
          
        </div>
        <div class="form-group">
          <label for="exampleFormControlFile1">Image</label>
          <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
          <input type="hidden" name="old_image" value="<?php echo $pages->image; ?>">
        </div>
        <div class="col-md-2 col-sm-4 text-center">
            <?php if(!empty($pages->image)){ ?> 
              <img src="<?php echo url('/'); ?>/gallery/<?php echo $pages->image; ?>" class="img-fluid" width="150">
              <?php } else{ ?>
              <img src="<?php echo url('/'); ?>/gallery/default.png" alt="" class="img-fluid" width="150">
              <?php
            }?>
        </div> 
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        
        </div>
      </div>
    </div>
@include('layouts.footer')