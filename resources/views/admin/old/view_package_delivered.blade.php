@include('layouts.left-side')
<script src="{{asset('/ckeditor/ckeditor.js')}}"> </script> 
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
      <li class="breadcrumb-item">
      <a href="<?php echo url('/'); ?>/admin/">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">
      <a href="<?php echo url('/'); ?>/admin/manage-package-to-be-delivered">Manage Package to be Delivered</a>
      </li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
        <i class="fas fa-users"></i>
        View Package to be Delivered</div>
        <div class="card-body">
          <div class="row">
            <div class="col-12">
             <div class="card">
                    <div class="card-block">
                      <div class="row">
                        <div class="col-md-2 col-sm-4 text-center">
                        <?php if(!empty($pages->image)){ ?> 
                          <img src="<?php echo url('/'); ?>/gallery/<?php echo $pages->image ?>" class="img-fluid" width="150">
                          <?php } else{ ?>
                          <img src="<?php echo url('/'); ?>/gallery/userdefault.png" alt="" class="img-fluid" width="150">
                          <?php
                        }?>

                        </div>
                        <div class="col-md-4 col-sm-8">
                          <h2 class="card-title">Title: <?php if(isset($pages->title) && !empty($pages->title)) { 
                          echo $pages->title; } ?></h2>
                         
                          <p class="card-text"><strong>Address: </strong> <?php echo $pages->address; ?> </p>
                          <p class="card-text"><strong>City: </strong> <?php echo $pages->city; ?> </p>
                          <p class="card-text"><strong>Zip Code: </strong> <?php echo $pages->zip_code; ?> </p>
                          <p class="card-text"><strong>Time: </strong> <?php echo $pages->time; ?> </p>
                          <p><strong>Status: </strong>
                          <?php if($pages->status == 1) {  ?>
                          <span class="badge bg-danger">Pending</span>
                          <?php } elseif($pages->status == 2) { ?>
                          <span class="badge bg-success">Delivered</span>
                          <?php } elseif($pages->status == 3) { ?>
                          <span class="badge bg-success">To be Delivered</span>
                          <?php }?>
                          </p>
                          <p class="card-text"><strong>Created Date: </strong> 
                          <?php $date = $pages->created_at; echo date('F d,Y',strtotime($date)); ?>
                          </p>
                        </div> 

                        <div class="col-md-2 col-sm-4 text-center">
                        <?php if(!empty($pages->dealerimage)){ ?> 
                          <img src="<?php echo url('/'); ?>/users/<?php echo $pages->dealerimage ?>" class="img-fluid" width="150">
                          <?php } else{ ?>
                          <img src="<?php echo url('/'); ?>/users/userdefault.png" alt="" class="img-fluid" width="150">
                          <?php
                        }?>

                        </div>
                        <div class="col-md-4 col-sm-8">
                          <h2 class="card-title">Title: <?php if(isset($pages->dealername) && !empty($pages->dealername)) { 
                          echo $pages->dealername; } ?></h2>
                         
                          <p class="card-text"><strong>City: </strong> <?php echo $pages->dealercity; ?> </p>
                          <p class="card-text"><strong>Email: </strong> <?php echo $pages->dealeremail; ?> </p>
                          <p class="card-text"><strong>Phone: </strong> <?php echo $pages->dealerphone; ?> </p>
                          
                        </div> 


                        <!-- User Details -->
                        <div class="col-md-4 col-sm-4 text-center">
                        <?php if(!empty($pages->userimage)){ ?> 
                          <img src="<?php echo url('/'); ?>/users/<?php echo $pages->userimage ?>" class="img-fluid" width="150">
                          <?php } else{ ?>
                          <img src="<?php echo url('/'); ?>/users/userdefault.png" alt="" class="img-fluid" width="150">
                          <?php
                        }?>

                        </div>
                        <div class="col-md-8 col-sm-8">
                          <h2 class="card-title">Title: <?php if(isset($pages->username) && !empty($pages->username)) { 
                          echo $pages->username; } ?></h2>
                         
                          <p class="card-text"><strong>City: </strong> <?php echo $pages->usercity; ?> </p>
                          <p class="card-text"><strong>Email: </strong> <?php echo $pages->useremail; ?> </p>
                          <p class="card-text"><strong>Phone: </strong> <?php echo $pages->userphone; ?> </p>
                          
                        </div> 
                        <div class="col-md-12 col-sm-12">
                          <h2 class="card-title">Billing Detail</h2>
                         
                          <p class="card-text"><strong>Card Details: </strong> <?php echo $pages->card_details; ?> </p>
                          <p class="card-text"><strong>Card expiry date: </strong> <?php echo $pages->card_expiry_date; ?> </p>
                          <p class="card-text"><strong>CVV: </strong> <?php echo $pages->cvv; ?> </p>
                          <p class="card-text"><strong>Street: </strong> <?php echo $pages->b_street; ?> </p>
                          <p class="card-text"><strong>City: </strong> <?php echo $pages->b_city; ?> </p>
                          <p class="card-text"><strong>Zip: </strong> <?php echo $pages->b_zip; ?> </p>
                          <p class="card-text"><strong>Confirmation Number: </strong> <?php echo $pages->confirmation_number; ?> </p>
                          <p class="card-text"><strong>Message: </strong> <?php echo $pages->message; ?> </p>
                          <p class="card-text"><strong>Time: </strong> <?php echo $pages->ptime; ?> </p>
                          
                        </div> 
                         
                        
                      </div>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
     <script type="text/javascript">
    CKEDITOR.replace( 'editor', {
            height: 300,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
  </script>
@include('layouts.footer')