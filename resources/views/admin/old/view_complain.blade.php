@include('layouts.left-side')
<script src="{{asset('/ckeditor/ckeditor.js')}}"> </script> 
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
      <li class="breadcrumb-item">
      <a href="<?php echo url('/'); ?>/admin/">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">
      <a href="<?php echo url('/'); ?>/admin/manage-complaints-list">Manage Complain</a>
      </li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
        <i class="fas fa-users"></i>
        View Complain</div>
        <div class="card-body">
          <div class="row">
            <div class="col-12">
             <div class="card">
                    <div class="card-block">
                      <div class="row">
                        
                        <div class="col-md-6 col-sm-8">
                          <h2 class="card-title">Title: <?php if(isset($pages->company_name) && !empty($pages->company_name)) { 
                          echo $pages->company_name; } ?></h2>
                         
                          <p class="card-text"><strong>Address: </strong> <?php echo $pages->company_address; ?> </p>
                          <p class="card-text"><strong>Street: </strong> <?php echo $pages->street; ?> </p>
                          <p class="card-text"><strong>City: </strong> <?php echo $pages->city; ?> </p>
                          <p class="card-text"><strong>State: </strong> <?php echo $pages->state; ?> </p>
                          <p class="card-text"><strong>Zip Code: </strong> <?php echo $pages->zip; ?> </p>
                          <p class="card-text"><strong>Type Entity: </strong> <?php echo $pages->type_entity; ?> </p>
                          
                          <p class="card-text"><strong>Corporation/DBA/LLC/Etc: </strong> <?php echo $pages->corporation; ?> </p>
                          <p class="card-text"><strong>EIN #/Social Security Number: </strong> <?php echo $pages->ein; ?> </p>
                          <p class="card-text"><strong>Number of years in Business: </strong> <?php echo $pages->years_business; ?> </p>
                          <p class="card-text"><strong>Created Date: </strong> 
                          <?php $date = $pages->created_at; echo date('F d,Y',strtotime($date)); ?>
                          </p>
                        </div> 
                         
                        
                      </div>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
     <script type="text/javascript">
    CKEDITOR.replace( 'editor', {
            height: 300,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
  </script>
@include('layouts.footer')