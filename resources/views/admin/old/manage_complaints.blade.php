@include('layouts.left-side')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script>
$(function() {
  $('input[name="daterange"]').daterangepicker({
    opens: 'left',
    locale: {
      format: 'YYYY-MM-DD'
    },
  }, function(start, end, label) {
    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});
</script>
      <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="<?php echo url('/'); ?>/admin/">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">
            <a href="<?php echo url('/'); ?>/admin/manage-complaints-list">Manage Complain List</a>
          </li>
        </ol>
        <form class="row" method="get" action="{{ action('AdminController@manage_complaints_list') }}">  
          <div class="form-group col-md-3">
          <input type="hidden" name="page" value="<?php echo @$_GET['page']; ?>">
          <label>Name</label>
          <input type="text" class="form-control" name="name" value="<?php echo @$_GET['name']; ?>" placeholder="Name">
          </div>
          <div class="form-group col-md-3">
          <label>Date Range</label>  
          <div class="input-group">
          <span class="input-group-prepend">
          <div class="input-group-text bg-transparent">
          <i class="fa fa-calendar"></i>
          </div>
          </span>
          <input class="form-control py-2" type="text" value=" " name="daterange" />
          </div>
          </div>
          <div class="form-group col-md-3">
          <label>Select status</label> 
          <select class="form-control"  name="status">
          <option <?php if(empty(@$_GET['status'])) echo "Selected"; ?>>Select status</option>
          <option <?php if(@$_GET['status'] == 1) echo "Selected"; ?> value="1">Active</option>
          <option <?php if(@$_GET['status'] == 2) echo "Selected"; ?> value="2">Inactive</option>
          </select>
          </div>
          <div class="form-group col-md-3 align-self-end">
          <button type="button" onclick="this.form.submit()" id="button-filter" class="btn btn-info"><i class="fa fa-filter"></i> Filter</button>
          </div>
        </form>
        <div class="row">
          <div class="form-group col-md-2">
            <form>
                <input type="hidden" name="page" value="<?php echo @$_GET['page']; ?>">
                <input type="hidden" name="name" value="<?php echo @$_GET['name']; ?>">
                <input type="hidden" name="daterange" value="<?php echo @$_GET['daterange']; ?>">
              <select class="form-control"  name="limit" onchange="this.form.submit()">
                <option <?php if(@$_GET['limit'] == 10) echo "Selected"; ?> >10</option>
                <option <?php if(@$_GET['limit'] == 20) echo "Selected"; ?> >20</option>
                <option <?php if(@$_GET['limit'] == 50) echo "Selected"; ?> >50</option>
                <option <?php if(@$_GET['limit'] == 100) echo "Selected"; ?> >100</option>
              </select>
            </form>
          </div> 
          <div class="form-group col-md-10">
              <h5>Item Per Page: Total Result Found: Showing 1 to {{ $pages->perPage() }}  of {{ $pages->total() }} ({{ $pages->lastPage() }} Pages)</h5>
          </div>
          <div class="col-md-12"> 
               {{ $pages->links() }} 
          </div>
        </div>
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-users"></i>
            Manage Complain List</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Company Name</th>
                    <th>City</th>
                    <th>Created Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i = 0;
                  foreach ($pages as $key => $value) {
                  $i++;
                  ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo ucfirst($value->company_name); ?>
                    
                    </td>
                    <td><?php echo $value->city; ?></td>
                    <td>
                      <?php $date = $value->created_at; 
                        echo date('F d,Y',strtotime($date));
                      ?>
                    </td>
                    <td>
                    <!-- <button class="btn btn-success btn-sm" title="Edit"><i class="fas fa-pen-square"></i></button> -->
                     <!-- <a class="btn btn-success btn-sm" role="menuitem" tabindex="-1" href="edit-package/<?php // echo $value->id?>"><i class="fas fa-pen-square"></i></a> -->
                     <a class="btn btn-info btn-sm" role="menuitem" tabindex="-1" href="view-complain/<?php echo $value->id?>"><i class="fas fa-eye"></i></a>
                     <a class="btn btn-danger btn-sm" role="menuitem" tabindex="-1" onclick="return confirm('Are you Sure Delete?');" href="remove-complain/<?php echo $value->id?>"><i class="fas fa-trash"></i>
                      </a>
                    </td>
                  </tr>
                  <?php 
                  } ?>
                </tbody>
              </table>
            </div>
          </div>
          <div class="col-md-12"> 
            {{ $pages->links() }} 
          </div>
          <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div> -->
        </div>
      </div>

@include('layouts.footer')