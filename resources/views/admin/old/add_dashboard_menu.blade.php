@include('layouts.left-side')
<script src="{{asset('/ckeditor/ckeditor.js')}}"> </script> 
      <script src="https://cdn.ckeditor.com/4.12.1/standard-all/ckeditor.js"></script>
      <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="<?php echo url('/'); ?>/admin/">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">
            <a href="<?php echo url('/'); ?>/admin/manage-menu">Manage Deshboard Menu</a>
          </li>
        </ol>
        
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-users"></i> Add Deshboard Menu
          </div>
          <div class="card-body">
            <form method="post" action="<?php echo url('/'); ?>/admin/new-dashboard-menu" enctype="multipart/form-data" role="form" id="page-form">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group">
                <label for="exampleInputEmail1">Title</label>
                <input type="text" class="form-control required" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Title" name="title">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Description</label>
                <!-- <textarea name="description" class="form-control required ckeditor" rows="7" cols="80"></textarea> -->
                <textarea cols="80" id="editor" name="editor1" rows="10" data-sample="1">
                </textarea>
              </div>
              <div class="form-group">
                <label for="exampleFormControlFile1">Image file input</label>
                <input type="file" name="picture" class="form-control-file required" id="exampleFormControlFile1">
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
      
 <script type="text/javascript">
    CKEDITOR.replace( 'editor', {
            height: 300,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
  </script>
      <script data-sample="1">
        CKEDITOR.replace( 'editor1', {
          fullPage: true,
          extraPlugins: 'docprops',
          // Disable content filtering because if you use full page mode, you probably
          // want to  freely enter any HTML content in source mode without any limitations.
          allowedContent: true,
          height: 320
        } );
      </script>
@include('layouts.footer')