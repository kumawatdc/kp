@include('layouts.left-side')
<script src="{{asset('/ckeditor/ckeditor.js')}}"> </script> 
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
      <li class="breadcrumb-item">
      <a href="<?php echo url('/'); ?>/admin/">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">
      <a href="<?php echo url('/'); ?>/admin/manage-department-menu">Disclaimer</a>
      </li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
        <i class="fas fa-users"></i>
        Disclaimer</div>
        <div class="card-body">
        <form method="post" action="<?php echo url('/'); ?>/admin/save-disclaimer" enctype="multipart/form-data" role="form" id="page-form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="<?php echo $disclaimer->id; ?>">
        <div class="form-group">
          <label for="exampleInputEmail1">English Disclaimer</label>
          <textarea name="title" id="editor" class="form-control required ckeditor" rows="7" cols="80"><?php echo $disclaimer->title; ?></textarea>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Portuguese Disclaimer</label>
          <textarea name="pt_title" id="pt_title" class="form-control required ckeditor" rows="7" cols="80"><?php echo $disclaimer->pt_title; ?></textarea>
        </div>
         <div class="form-group">
          <label for="exampleInputEmail1">Polish Disclaimer</label>
          <textarea name="ps_title" id="ps_title" class="form-control required ckeditor" rows="7" cols="80"><?php echo $disclaimer->ps_title; ?></textarea>
        </div>
         <div class="form-group">
          <label for="exampleInputEmail1">Spanish Disclaimer</label>
          <textarea name="es_title" id="es_title" class="form-control required ckeditor" rows="7" cols="80"><?php echo $disclaimer->es_title; ?></textarea>
        </div>
        <!-- <div class="form-group">
          <label for="exampleFormControlFile1">Image</label>
          <input type="file" name="images" class="form-control-file required" id="exampleFormControlFile1">
        </div> -->
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      CKEDITOR.replace( 'editor', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
      CKEDITOR.replace( 'pt_title', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
      CKEDITOR.replace( 'ps_title', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
      CKEDITOR.replace( 'es_title', {
            height: 200,
            filebrowserUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=file",
            filebrowserImageUploadUrl: "http://localhost/Jaipuruz/public/ajaxfile.php?type=image"
        } );
    </script>
@include('layouts.footer')