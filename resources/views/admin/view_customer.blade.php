@include('layouts.left-side')
<div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Customer base</h1>
                        <!-- <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Customer base</li>
                        </ol> -->
                        <div class="row">
                        <div class="col-xl-12 col-md-12 mb-4">
                              <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                  <div class="row no-gutters align-items-center">
                                    <table class="table table-bordered" id="" width="100%" cellspacing="0">
                                      <thead>
                                        <tr>
                                          <th>ID</th>
                                          <th>Name</th>
                                          <th>Phone</th>
                                          <th>Image</th>
                                          <th>Status</th>
                                          <th>Created Date</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                          <tr>
                                              <td><?php echo $pages->id; ?></td>
                                              <td><?php echo ucfirst($pages->name); ?></td>
                                              <td><?php echo $pages->phone; ?></td>
                                              <td>
                                               <?php if(isset($pages->image) && !empty($pages->image)) {
                                                $image = $pages->image;
                                              }else{
                                                $image = "userdefault.png";
                                              } ?>
                                              <img width="50" height="50" class="rounded-circle" src="<?php echo url('/'); ?>/users/<?php echo $image; ?>">
                                              </td>
                                              <td>
                                                <div class="dropdown">
                                                  <?php if($pages->status == 1) {  ?>
                                                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">
                                                      Approved
                                                    </button>
                                                    <div class="dropdown-menu">
                                                      <a onclick="return confirm('Are you Sure Decline');" class="dropdown-item" role="menuitem" tabindex="-1" href="<?php echo url('/'); ?>/admin/update-users/<?php echo $pages->id."/".$pages->status?>"><i class="fa fa-close"></i> Decline</a>
                                                    </div>
                                                  <?php
                                                  }else{?>
                                                     <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">
                                                      Pending
                                                    </button>
                                                    <div class="dropdown-menu">
                                                      <a onclick="return confirm('Are you Sure Approve');" class="dropdown-item" role="menuitem" tabindex="-1" href="<?php echo url('/'); ?>/admin/update-users/<?php echo $pages->id."/".$pages->status?>"><i class="fa fa-check"></i> Approved</a>
                                                    </div>
                                                  <?php
                                                  }?>
                                                </div>
                                              </td>
                                              <td>
                                                <?php $date = $pages->created_at; 
                                                  echo date('F d,Y',strtotime($date));
                                                ?>
                                              </td>
                                          </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                        </div>
 
                    </div>
                </main>
@include('layouts.footer')