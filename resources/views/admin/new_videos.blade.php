@include('layouts.left-side')
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
          <!-- <a href="<?php echo url('/'); ?>/admin/new-videos">Add New Video</a> -->
            <h1 class="mt-4">Videos</h1>
            <!-- <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Customer base</li>
            </ol> -->
            <div class="row">
            <div class="col-xl-12 col-md-12 mb-4">
                  <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                      <div class="row no-gutters align-items-center">
                        
                        <form method="post" action="<?php echo url('/'); ?>/admin/add-new-video" enctype="multipart/form-data" role="form" id="page-form">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          
                          
                          <div class="form-group">
                            <label for="exampleFormControlFile1">Image</label>
                            <input type="file" name="images" class="form-control-file required" id="exampleFormControlFile1">
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlFile1">Video</label>
                            <input type="file" name="video" class="form-control-file required" id="exampleFormControlFile1">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input type="text" class="form-control required" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Title" name="title">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Album</label>
                            <select class="form-control" id="gallery" name="gallery" required="">
                              <option value="">Please Select Album</option>
                              <?php foreach ($pages as $key => $value) { ?>
                                    <option value="<?php echo @$value->id; ?>"><?php echo @$value->title; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="form-group">
                            <input type="hidden" name="downloading" value="0" />
                            <input type="checkbox" name="downloading" value="1"> Possibility for Downloading
                          </div>
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                      </div>
                    </div>
                  </div>
            </div>

        </div>
    </main>
@include('layouts.footer')