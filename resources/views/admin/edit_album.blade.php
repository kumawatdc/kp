@include('layouts.left-side')

<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
          <a class="btn btn-primary mt-4" href="<?php echo url('/'); ?>/admin/new-album">Add New Video</a>
            <h1 class="mt-4">Album</h1>
            <!-- <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Customer base</li>
            </ol> -->
            <div class="row">
            <div class="col-xl-12 col-md-12 mb-4">
                  <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                      <div class="row no-gutters align-items-center">
                        
                        <form method="post" action="<?php echo url('/'); ?>/admin/update-album" enctype="multipart/form-data" role="form" id="page-form">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="id" value="<?php echo $pages->id; ?>">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input type="text" class="form-control required" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Title" value="<?php echo $pages->title; ?>" name="title" required="">
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlFile1">Image</label>
                            <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                            <input type="hidden" name="old_image" value="<?php echo $pages->image; ?>">
                          </div>
                          <div class="col-md-6 col-sm-4 text-center">
                              <?php if(!empty($pages->image)){ ?> 
                                <img src="<?php echo url('/'); ?>/gallery/<?php echo $pages->image; ?>" class="img-fluid" width="150">
                                <?php } else{ ?>
                                <img src="<?php echo url('/'); ?>/gallery/default.png" alt="" class="img-fluid" width="150">
                                <?php
                              }?>
                          </div> 
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                      </div>
                    </div>
                  </div>
            </div>

        </div>
    </main>

@include('layouts.footer')