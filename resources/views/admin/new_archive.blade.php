@include('layouts.left-side')
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Archive</h1>
            <!-- <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Customer base</li>
            </ol> -->
            <div class="row">
            <div class="col-xl-12 col-md-12 mb-4">
                  <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                      <div class="row no-gutters align-items-center">
                        
                        <form method="post" action="<?php echo url('/'); ?>/admin/add-new-archive" enctype="multipart/form-data" role="form" id="page-form">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          
                          
                          <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" class="form-control required" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Name" name="name">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Password</label>
                            <input type="password" class="form-control required" id="exampleInputpassword" aria-describedby="passwordHelp" placeholder="Password" name="password">
                          </div>
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                      </div>
                    </div>
                  </div>
            </div>

        </div>
    </main>
@include('layouts.footer')