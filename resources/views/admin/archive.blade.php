@include('layouts.left-side')
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
          <a href="<?php echo url('/'); ?>/admin/new-archive">Add New Archive</a>
            <h1 class="mt-4">Archive</h1>
            <!-- <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Customer base</li>
            </ol> -->
            <div class="row">
            <div class="col-xl-12 col-md-12 mb-4">
                  <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                      <div class="row no-gutters align-items-center">
                        <table class="table table-bordered" id="" width="100%" cellspacing="0">
                          <thead>
                            <tr>
                              <th>ID</th>
                              <th>Name</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $i = 0;
                            foreach ($pages as $key => $value) {
                            $i++;
                            ?>
                              <tr>
                                  <td><?php echo $value->id; ?></td>
                                  <td><?php echo ucfirst($value->name); ?></td>
                                  <td>
                                    <!-- <button class="btn btn-success btn-sm" title="Edit"><i class="fas fa-pen-square"></i></button> -->

                                     <a class="btn btn-success btn-sm" role="menuitem" tabindex="-1" href="edit-archive/<?php echo $value->id?>"><i class="fas fa-pen-square"></i></a>
                                     <a class="btn btn-danger btn-sm" role="menuitem" tabindex="-1" onclick="return confirm('Are you Sure Delete?');" href="remove-archive/<?php echo $value->id?>"><i class="fas fa-trash"></i>
                                      </a> 
                                  </td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
            </div>

        </div>
    </main>
@include('layouts.footer')