@include('layouts.left-side')

<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
          
            <h1 class="mt-4">Helping Customers</h1>
            <!-- <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Customer base</li>
            </ol> -->
            <div class="row">
            <div class="col-xl-12 col-md-12 mb-4">
                  <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                      <div class="row no-gutters align-items-center">
                        
                        <form method="post" action="<?php echo url('/'); ?>/admin/update-helping-customer" enctype="multipart/form-data" role="form" id="page-form">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="id" value="<?php echo $pages->id; ?>">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" class="form-control required" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email" value="<?php echo $pages->email; ?>" name="email">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Phone</label>
                            <input type="phone" class="form-control required" id="exampleInputphone" aria-describedby="phoneHelp" placeholder="Phone" value="<?php echo $pages->phone; ?>" name="phone">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputAddress">Address</label>
                            <input type="text" class="form-control required" id="exampleInputAddress" aria-describedby="emailHelp" placeholder="Address" value="<?php echo $pages->address; ?>" name="address">
                          </div>
                          
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                      </div>
                    </div>
                  </div>
            </div>

        </div>
    </main>

@include('layouts.footer')