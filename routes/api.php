<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->namespace('API')->group(function () {
	Route::post('/login', 'WebserviceController@login');
	Route::post('/check-otp', 'WebserviceController@check_otp');
	Route::post('/video-qrcode', 'WebserviceController@video_qrcode');
	Route::post('/video-download', 'WebserviceController@video_download');
	Route::post('/downloaded-list', 'WebserviceController@downloaded_list');
	Route::get('/instructions', 'WebserviceController@instructions');
	Route::get('/help', 'WebserviceController@help');

	//--Old--//
	Route::post('/forgot-password', 'WebserviceController@forgot_password');
	Route::post('/profile', 'WebserviceController@profile');

	//});
});
// Route::middleware('auth:api')->get('/api', function (Request $request) {
// 	Route::group(['prefix' => 'api'], function() {
// 		Route::post('/login', 'WebserviceController@login');
//     	//return $request->user();
// 	});
// });
