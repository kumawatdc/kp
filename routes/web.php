<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', 'AdminController@index');
Route::get('/home', 'AdminController@index');
Route::get('/admin', 'AdminController@index');
// Route::get('/', function () {
//     return view('welcome');
// });
Route::group(['prefix' => 'admin'], function() {
	Route::get('/', 'AdminController@index');
	//--Manage Users--//
	//new code
	Route::get('/statistics', 'AdminController@statistics');
	Route::get('/customer-base', 'AdminController@customer_base');
	Route::get('/view-customer/{id}', 'AdminController@view_customer');
	Route::get('/update-users/{id}/{status}', 'AdminController@update_users');
	Route::get('/videos', 'AdminController@videos');
	Route::get('/new-videos', 'AdminController@new_videos');
	Route::post('/add-new-video', 'AdminController@add_new_video');
	Route::get('/edit-video/{id}', 'AdminController@edit_video');
	Route::post('/update-video', 'AdminController@update_video');
	Route::get('/remove-video/{id}', 'AdminController@remove_video');
	Route::get('/album', 'AdminController@gallery');
	Route::get('/new-album', 'AdminController@new_gallery');
	Route::post('/add-new-album', 'AdminController@add_new_gallery');
	Route::get('/edit-album/{id}', 'AdminController@edit_gallery');
	Route::post('/update-album', 'AdminController@update_gallery');
	Route::get('/remove-album/{id}', 'AdminController@remove_gallery');
	

	// Route::get('/archive', 'AdminController@archive');
	// Route::get('/new-archive', 'AdminController@new_archive');
	// Route::post('/add-new-archive', 'AdminController@add_new_archive');
	// Route::get('/edit-archive/{id}', 'AdminController@edit_archive');
	// Route::post('/update-archive', 'AdminController@update_archive');
	// Route::get('/remove-archive/{id}', 'AdminController@remove_archive');
	
	Route::get('/archive', 'AdminController@narchive');
	Route::get('/edit-archive/{id}', 'AdminController@edit_narchive');
	Route::post('/update-archive', 'AdminController@update_narchive');
	Route::get('/remove-archive/{id}', 'AdminController@remove_narchive');


	Route::get('/helping-customers', 'AdminController@helping_customers');
	Route::post('/update-helping-customer', 'AdminController@update_helping_customer');

	Route::get('/instructions', 'AdminController@instructions');
	Route::post('/update-instructions', 'AdminController@update_instructions');



});