<?php

namespace App\Http\Controllers;

use App\Http\Requests\adminFormRequest;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use App\Components\FlashMessages;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Session;
use Carbon\Carbon;
class AdminController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth', ['except' => ['forgot_password', 'save_password','tesing']]);
    }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    // This function use for Admin-setting Start --

    // This function use for Admin- Dashboard START --
    public function index(Request $request)
    {
      $totalusers = DB::table('users')
            ->count(); 
      $todayusers = DB::table('users')
            ->whereDate('created_at', Carbon::today())->count();
      $currentyear = date('Y');
      $data = DB::table("users")
            ->whereRaw('YEAR(created_at) = ?',[$currentyear])
            ->get();
      $currentMonth = date('m');
      $datam = array();
      for($i = 1; $i <= $currentMonth; $i++){
      $datam[] = DB::table("users")
              ->whereRaw('YEAR(created_at) = ?',[$currentyear])
              ->whereRaw('MONTH(created_at) = ?',[$i])
              ->count();
      }
      //$datam = array_combine(range(1, count($datam)), array_values($datam));
      $active = "Dashboard";
      $active_menu = "Dashboard";
      return view('admin/index', compact('active','active_menu','totalusers','todayusers','data','datam'));
    }
    // This function use for Admin- Dashboard END --

    public function statistics()
    {
      $pages = DB::table('users')
                ->get();
      $totalusers = DB::table('users')
            ->count();
      $todayusers = DB::table('users')
            ->whereDate('created_at', Carbon::today())->count();
      $totaldownvideo = DB::table('download_video')
            ->whereDate('created_at', Carbon::today())->count();
      $totalwatchvideo = DB::table('watch_video')
            ->whereDate('created_at', Carbon::today())->count();
      $lastsixmonth = DB::table('users')
            ->where("created_at",">", Carbon::now()->subMonths(6))->get();
      $datamonth =  DB::table('users')
                  ->whereYear('created_at', '>=', Carbon::now()->subMonths(6))
                  ->groupBy('month')
                  ->orderBy('month', 'ASC')
                  ->get([
                      DB::raw('MONTH(created_at) as month'),
                      DB::raw('COUNT(*) as value')
                  ]);
      $dwnlvideomonth =  DB::table('download_video')
                  ->whereYear('created_at', '>=', Carbon::now()->subMonths(6))
                  ->groupBy('month')
                  ->orderBy('month', 'ASC')
                  ->get([
                      DB::raw('MONTH(created_at) as month'),
                      DB::raw('COUNT(*) as value')
                  ]);
      $watchvideomonth =  DB::table('watch_video')
                  ->whereYear('created_at', '>=', Carbon::now()->subMonths(6))
                  ->groupBy('month')
                  ->orderBy('month', 'ASC')
                  ->get([
                      DB::raw('MONTH(created_at) as month'),
                      DB::raw('COUNT(*) as value')
                  ]);
      $watchvideomontharray = array();
      $watchvideovideovaluearray = array();
      foreach ($watchvideomonth as $key2 => $value2) {
        $watchvideomontharray[] = date("F", mktime(0, 0, 0, $value2->month, 1));
        $watchvideovideovaluearray[] = $value2->value;
      }
      //echo "<pre>"; print_r($watchvideovideovaluearray); die;
      $dwnlvideomontharray = array();
      $dwnlvideovaluearray = array();
      foreach ($dwnlvideomonth as $key1 => $value1) {
        $dwnlvideomontharray[] = date("F", mktime(0, 0, 0, $value1->month, 1));
        $dwnlvideovaluearray[] = $value1->value;
      }

      $montharray = array();
      $valuearray = array();
      foreach ($datamonth as $key => $value) {
        $montharray[] = date("F", mktime(0, 0, 0, $value->month, 1));
        $valuearray[] = $value->value;
      }
      

      $josninmo = json_encode($montharray);
      $josnvalue = json_encode($valuearray);
      $josndwnlvideo = json_encode($dwnlvideomontharray);
      $josndwnlvideovalue = json_encode($dwnlvideovaluearray);
      $josnwatchvideo = json_encode($watchvideomontharray);
      $josnwatchvideovalue = json_encode($watchvideovideovaluearray);
      //echo "<pre>"; print_r($datamonth); print_r($montharray); die;
      $active = "statistics"; 
        return view('admin/statistics',compact('pages','active','totalusers','todayusers','josninmo','josnvalue','totaldownvideo','josndwnlvideo','josndwnlvideovalue','totalwatchvideo','josnwatchvideo','josnwatchvideovalue'));
    }

    public function customer_base()
    {
      $user = Auth::user();
      $permission = $user->permission;
      if($user->role == "1"){ 
        $paginationlimit = '10';
        $pages = DB::table('users')
                ->orderBy('id','DESC')
                ->where('role', '1');
        if(isset($_GET['limit']) && !empty($_GET['limit'])){
          $paginationlimit = $_GET['limit'];
        }
        if(isset($_GET['name']) && !empty($_GET['name'])){
          $name = $_GET['name'];
          $pages = $pages->where('name', 'like', "%$name%")
                  ->orwhere('email', 'like', "%$name%")
                  ->orwhere('city', 'like', "%$name%");

        } 
        if(isset($_GET['daterange']) && !empty($_GET['daterange'])){
          $daterange = $_GET['daterange'];
          //echo $daterange; die;
          $daterange = explode(" - ", $daterange);
          $startdate = $daterange[0];
          $enddate = $daterange[1];
          $today = date("Y-m-d");
          if($startdate != $today){
            // echo $startdate."==".$enddate; die;
            $pages = $pages->WhereBetween('created_at',[$startdate, $enddate]);
          }
        }
        if(isset($_GET['status']) && !empty($_GET['status'])){
          $status = $_GET['status'];
          if($status != "Select status"){
            if($status == 2){
              $status = 0;
            }else{
              $status = 1;
            }
            // echo $startdate."==".$enddate; die;
            $pages = $pages->Where('status',$status);
          }
        }
        
        $pages = $pages->paginate($paginationlimit);
        //echo "<pre>"; print_r($pages); die;
        $active = "Customer Base";
        return view('admin/customer_base',compact('pages','active'));
      }else{
          return Redirect::to('/');
      }
    }

    public function view_customer($id)
    {
      $pages = DB::table('users')
                ->where('id',$id)
                ->first();
      $active = "Customer Base"; 
      return view('admin/view_customer',compact('pages','active'));
    }

    public function update_users(Request $request,$id,$status)
    {
      if($status == 1){
      $status1 = "0";
      }else{
      $status1 = "1";
      }
      $update=DB::table('users')
      ->where('id', $id)
      ->update(array(
      'status' => $status1
      ));
      $request->session()->flash('success', 'Your Status has been updated');
      return redirect()->back();
    }

    public function videos()
    {
      $pages = DB::table('videos')
                ->get();
      $active = "Video"; 
      return view('admin/videos',compact('pages','active'));
    }

    public function new_videos()
    {
       $pages = DB::table('gallery')
                 ->get();
      $active = "Video"; 
      return view('admin/new_videos',compact('active','pages'));
    }

    public function add_new_video(Request $request)
    {
      $title            = (@$_POST['title']) ?: '';
      $gallery            = (@$_POST['gallery']) ?: '';
      $downloading      = (@$_POST['downloading']) ?: '0';
      $today            = date("Y-m-d");
      $today1 = date("YmdHis"); 
      if(!empty($_FILES['images']['name'])){
          $image = $_FILES['images']['name'];
          $temp_anme = $_FILES["images"]["tmp_name"];
          $ext = pathinfo($image, PATHINFO_EXTENSION);
          $name1 = "photo_".$today1."_".rand(4,99999).".".$ext;
          $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/Jaipuruz/public/gallery/';
          $imagePath = $uploadfile. $name1;
          move_uploaded_file($temp_anme,$imagePath);
          $hm_image = $name1;
      }else{
        $hm_image = "";
      }
      //echo $hm_image; die;
      if(!empty($_FILES['video']['name'])){
          $video = $_FILES['video']['name'];
          $temp_anme1 = $_FILES["video"]["tmp_name"];
          $ext1 = pathinfo($video, PATHINFO_EXTENSION);
          $name12 = "video_".$today1."_".rand(4,99999).".".$ext1;
          $uploadfile1 = $_SERVER['DOCUMENT_ROOT'] . '/Jaipuruz/public/gallery/';
          $imagePath1 = $uploadfile1. $name12;
          move_uploaded_file($temp_anme1,$imagePath1);
          $hm_image1 = $name12;
      }else{
        $hm_image1 = "";
      }

      
      $update=DB::table('videos')
          ->insert(array(
          'title'           => $title,
          'gallery_id'      => $gallery,
          'photo'           => $hm_image,
          'video'           => $hm_image1,
          'download_satus'  => $downloading,
          'created_at'      => $today
      ));
      $request->session()->flash('success', 'Your Video has been added');
      return Redirect::to('/admin/videos');
    }

    public function edit_video($id)
    {
      $pages = DB::table('videos')
                ->leftJoin('gallery','gallery.id', '=' ,'videos.gallery_id')
                ->select('videos.*','gallery.title as gallerytitle')
                ->orderBy('videos.id','DESC')
                ->where('videos.id',$id)
                ->first();
      $gallery = DB::table('gallery')
                 ->get();
      $active = "Video";
        return view('admin/edit_video',compact('pages','active','gallery'));
    }

    public function update_video(Request $request)
    {
      $id     = $_POST['id'];
      $title  = $_POST['title'];
      $gallery  = $_POST['gallery'];
      //if($title != 'Gallery'){
      $downloading          = $_POST['downloading'];
      $old_video  = $_POST['old_video'];
      $old_image  = $_POST['old_image'];
      $today1 = date("YmdHis"); 
        if(!empty($_FILES['image']['name'])){
            $image = $_FILES['image']['name'];
            $temp_anme = $_FILES["image"]["tmp_name"];
            $ext = pathinfo($image, PATHINFO_EXTENSION);
            $name = "video_".$today1."_".rand(4,99999).".".$ext;
            $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/Jaipuruz/public/gallery/';
            $imagePath = $uploadfile. $name;
            move_uploaded_file($temp_anme,$imagePath);
            @unlink($uploadfile.$old_image);
            $hm_image = $name;
        }else{
            $hm_image = $old_image;
        }

        if(!empty($_FILES['video']['name'])){
          $video = $_FILES['video']['name'];
          $temp_anme1 = $_FILES["video"]["tmp_name"];
          $ext1 = pathinfo($video, PATHINFO_EXTENSION);
          $name12 = "video_".$today1."_".rand(4,99999).".".$ext1;
          $uploadfile1 = $_SERVER['DOCUMENT_ROOT'] . '/Jaipuruz/public/gallery/';
          $imagePath1 = $uploadfile1. $name12;
          move_uploaded_file($temp_anme1,$imagePath1);
          @unlink($uploadfile1.$old_video);
          $hm_image1 = $name12;
        }else{
          $hm_image1 = $old_video;
        }
        $today = date("Y-m-d"); 
        $update=DB::table('videos')
            ->where('id',$id)
            ->update(array(
            'title'           => $title,
            'gallery_id'      => $gallery,
            'photo'           => $hm_image,
            'video'           => $hm_image1,
            'download_satus'  => $downloading,
        ));
      //}
      
      $request->session()->flash('success', 'Your Video has been updated');
      return Redirect::to('/admin/videos');
    }

    public function remove_video(Request $request,$id)
    {
      $pages = DB::table('videos')
                ->where('id',$id)
                ->first();
      $uploadfile1 = $_SERVER['DOCUMENT_ROOT'] . '/Jaipuruz/public/gallery/';         
      $image = $pages->photo;
      $video = $pages->video;
      @unlink($uploadfile1.$image);
      @unlink($uploadfile1.$video);
      // $teams = $_POST['clients'];
      // $teams=DB::table('users')->whereIn('id', $teams)->delete();
      //echo "<pre>"; print_r($_POST); die;
      $teams=DB::table('videos')->where('id', $id)->delete();
      $request->session()->flash('success', 'Your Video has been deleted');
      return redirect()->back();
    }

    // Gallery start
    public function gallery()
    {
      $pages = DB::table('gallery')
                ->get();
      $active = "Gallery"; 
      return view('admin/album',compact('pages','active'));
    }
    public function new_gallery()
    {
      // $pages = DB::table('videos')
      //           ->get();
      $active = "Gallery"; 
      return view('admin/new_album',compact('active'));
    }

    public function add_new_gallery(Request $request)
    {
      $title            = (@$_POST['title']) ?: '';
      $today            = date("Y-m-d");
      $today1 = date("YmdHis"); 
      if(!empty($_FILES['images']['name'])){
          $image = $_FILES['images']['name'];
          $temp_anme = $_FILES["images"]["tmp_name"];
          $ext = pathinfo($image, PATHINFO_EXTENSION);
          $name1 = "photo_".$today1."_".rand(4,99999).".".$ext;
          $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/Jaipuruz/public/gallery/';
          $imagePath = $uploadfile. $name1;
          move_uploaded_file($temp_anme,$imagePath);
          $hm_image = $name1;
      }else{
        $hm_image = "";
      }
      //echo $hm_image; die;
      $update=DB::table('gallery')
          ->insert(array(
          'title'           => $title,
          'image'           => $hm_image,
          'created_at'      => $today
      ));
      $request->session()->flash('success', 'Your Video has been added');
      return Redirect::to('/admin/album');
    }
    public function edit_gallery($id)
    {
      $pages = DB::table('gallery')
                ->where('id',$id)
                ->first();
      $active = "Gallery";
        return view('admin/edit_album',compact('pages','active'));
    }
    public function update_gallery(Request $request)
    {
      $id     = $_POST['id'];
      $title  = $_POST['title'];
      //if($title != 'Gallery'){
      $old_image  = $_POST['old_image'];
      $today1 = date("YmdHis"); 
        if(!empty($_FILES['image']['name'])){
            $image = $_FILES['image']['name'];
            $temp_anme = $_FILES["image"]["tmp_name"];
            $ext = pathinfo($image, PATHINFO_EXTENSION);
            $name = "video_".$today1."_".rand(4,99999).".".$ext;
            $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/Jaipuruz/public/gallery/';
            $imagePath = $uploadfile. $name;
            move_uploaded_file($temp_anme,$imagePath);
            @unlink($uploadfile.$old_image);
            $hm_image = $name;
        }else{
            $hm_image = $old_image;
        }

        $today = date("Y-m-d"); 
        $update=DB::table('gallery')
            ->where('id',$id)
            ->update(array(
            'title'           => $title,
            'image'           => $hm_image,
        ));
      //}
      
      $request->session()->flash('success', 'Your Gallery has been updated');
      return Redirect::to('/admin/album');
    }

    public function remove_gallery(Request $request,$id)
    {
      $pages = DB::table('gallery')
                ->where('id',$id)
                ->first();
      $uploadfile1 = $_SERVER['DOCUMENT_ROOT'] . '/Jaipuruz/public/gallery/';         
      $image = $pages->image;
      @unlink($uploadfile1.$image);
      // $teams = $_POST['clients'];
      // $teams=DB::table('users')->whereIn('id', $teams)->delete();
      //echo "<pre>"; print_r($_POST); die;
      $teams=DB::table('gallery')->where('id', $id)->delete();
      $request->session()->flash('success', 'Your Gallery has been deleted');
      return redirect()->back();
    }

    public function helping_customers()
    {
      $pages = DB::table('helping_customers')
                ->first();
      $active = "Helping Customers";
        return view('admin/helping_customers',compact('pages','active'));
    }

    public function update_helping_customer(Request $request)
    {
      $id       = $_POST['id'];
      $email    = $_POST['email'];
      $phone    = $_POST['phone'];
      $address  = $_POST['address'];
        
        $update=DB::table('helping_customers')
            ->where('id',$id)
            ->update(array(
            'email'           => $email,
            'phone'           => $phone,
            'address'           => $address
        ));
      $request->session()->flash('success', 'Your Helping Customer has been updated');
      return Redirect::to('/admin/helping-customers');
    }

    public function archive()
    {
      $user = Auth::user();
      $permission = $user->permission;
      if($user->role == "1"){ 
        $paginationlimit = '10';
        $pages = DB::table('users')
                ->orderBy('id','DESC')
                ->where('role', '2');
        if(isset($_GET['limit']) && !empty($_GET['limit'])){
          $paginationlimit = $_GET['limit'];
        }
        if(isset($_GET['name']) && !empty($_GET['name'])){
          $name = $_GET['name'];
          $pages = $pages->where('name', 'like', "%$name%")
                  ->orwhere('email', 'like', "%$name%")
                  ->orwhere('city', 'like', "%$name%");

        } 
        if(isset($_GET['daterange']) && !empty($_GET['daterange'])){
          $daterange = $_GET['daterange'];
          //echo $daterange; die;
          $daterange = explode(" - ", $daterange);
          $startdate = $daterange[0];
          $enddate = $daterange[1];
          $today = date("Y-m-d");
          if($startdate != $today){
            // echo $startdate."==".$enddate; die;
            $pages = $pages->WhereBetween('created_at',[$startdate, $enddate]);
          }
        }
        if(isset($_GET['status']) && !empty($_GET['status'])){
          $status = $_GET['status'];
          if($status != "Select status"){
            if($status == 2){
              $status = 0;
            }else{
              $status = 1;
            }
            // echo $startdate."==".$enddate; die;
            $pages = $pages->Where('status',$status);
          }
        }
        
        $pages = $pages->paginate($paginationlimit);
        //echo "<pre>"; print_r($pages); die;
        $active = "Archive";
        return view('admin/archive',compact('pages','active'));
      }else{
          return Redirect::to('/');
      }
    }

    public function new_archive()
    {
      // $pages = DB::table('videos')
      //           ->get();
      $active = "Archive"; 
      return view('admin/new_archive',compact('active'));
    }

    public function add_new_archive(Request $request)
    {
      $name            = (@$_POST['name']) ?: '';
      $password1      = (@$_POST['password']) ?: '';
      $password = Hash::make($password1);
      $today            = date("Y-m-d");
      $update=DB::table('users')
          ->insert(array(
          'name'           => $name,
          'role'           => '2',
          'password'        => $password,
          'created_at'      => $today
      ));
      $request->session()->flash('success', 'Your Archive has been added');
      return Redirect::to('/admin/archive');
    }

    public function edit_archive($id)
    {
      $pages = DB::table('users')
                ->where('id',$id)
                ->first();
      $active = "Archive";
        return view('admin/edit_archive',compact('pages','active'));
    }

    public function update_archive(Request $request)
    {
      $id       = $_POST['id'];
      $name    = $_POST['title'];
      $password1    = $_POST['password'];
      if(isset($password1) && !empty($password1)){
        $password = Hash::make($password1);
      }else{
        $password = $_POST['old_password'];
      }
        
        $update=DB::table('users')
            ->where('id',$id)
            ->update(array(
            'name'           => $name,
            'password'           => $password
        ));
      $request->session()->flash('success', 'Your Archive has been updated');
      return Redirect::to('/admin/archive');
    }

    public function remove_archive(Request $request,$id)
    {
      
      // $teams = $_POST['clients'];
      // $teams=DB::table('users')->whereIn('id', $teams)->delete();
      //echo "<pre>"; print_r($_POST); die;
      $teams=DB::table('users')->where('id', $id)->delete();
      $request->session()->flash('success', 'Your Archive has been deleted');
      return redirect()->back();
    }


    public function instructions()
    {
      $pages = DB::table('instructions')
                ->first();
      $active = "instructions";
        return view('admin/instructions',compact('pages','active'));
    }

    public function update_instructions(Request $request)
    {
      $id       = $_POST['id'];
      $text    = $_POST['text'];
        
        $update=DB::table('instructions')
            ->where('id',$id)
            ->update(array(
            'text'           => $text
        ));
      $request->session()->flash('success', 'Your instructions has been updated');
      return Redirect::to('/admin/instructions');
    }

//old code


    //old functions..//
    public function profile()
    {
      $user = Auth::user();
          $admins = DB::table('admin')->where('id',$user->id)->first();
      $active = "";
      $active_menu = "ADMIN";
      return view('admin/profile',compact('admins','active','active_menu'));
    }

    public function update_profile(Request $request)
    {
      //echo "<pre>"; print_r($_POST); die;
      $admin_first_name = $_POST['name'];
      $admin_email      = $_POST['email'];
      $admin_password   = $_POST['password'];
      $old_pass         = $_POST['old_pass'];
      if(!empty($admin_password)){
        $pass  = Hash::make($admin_password);
      }else{
         $pass = $old_pass;
      }
      $user = Auth::user();
      $userid = $user->id;
      $update=DB::table('admin')
          ->where('id', $userid)
          ->update(array(
          'name'=> $admin_first_name,
          'email' => $admin_email,
          'password'  => $pass
      ));
      $request->session()->flash('success', 'Your profile has been updated!');
      return Redirect::to('/profile');
    }
    public function upload_profile()
    {
      //echo "kp"; die;
      //echo "<pre>"; print_r($_FILES); die;
      $old_image  = $_POST['old_image'];
      if(!empty($_FILES['admin_picture']['name'])){
          $image = $_FILES['admin_picture']['name'];
          $temp_anme = $_FILES["admin_picture"]["tmp_name"];
          $name = rand(10,99999).$image;
           $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/my-anjels/public/users/';
           //$uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/jatt_juliet/public/users/';
          //echo $uploadfile; die;
          //$destinationPath = url('/').'/home/';
          $imagePath = $uploadfile. $name;
          //echo $temp_anme."<br>".$imagePath; die;
          move_uploaded_file($temp_anme,$imagePath);
          @unlink($uploadfile.$old_image);
          $hm_image = $name;
      }else{
          $hm_image = $old_image;
      }
      $user = Auth::user();
      $userid = $user->id;
      $update=DB::table('admin')
          ->where('id', $userid)
          ->update(array(
          'picture'      => $hm_image
      ));
      return $hm_image; 
      //return Redirect::to('/profile');
    }
    public function forgot_password(Request $request){
       return view('admin/email');
    }
    public function save_password(Request $request){
      //echo "<pre>"; print_r($_POST); die;
      $email=$_POST['email'];
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $password1 = '';
      $length = 8;
      for ($i = 0; $i < $length; $i++) {
          $password1 .= $characters[rand(0, $charactersLength - 1)];
      }
      $password  = Hash::make($password1);
      // $password = md5($password1);
      $admins = DB::table('admin')->where('email',$email)->first();
      if(isset($admins) && !empty($admins)) {
        $admins->name;
        $password1;
        $user_text = "<html><head></head><body style='font-family: Arial; font-size: 12px;'><div>
        Hello $admins->name<p>
        You have requested a Forget password, Your new password $password1</p><p>
        Please ignore this email if you did not request a gorget password.</p></div></body></html>";
        //echo $user_text; die;
        $to = $email;
        $subject = 'Forgot-password my-anjels';
        $from = $email;
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: '.$from."\r\n".
                  'Reply-To: '.$from."\r\n" .
                  'X-Mailer: PHP/' . phpversion();
        mail($to, $subject, $user_text, $headers);
        $update=DB::table('admin')
              ->where('email', $email)
              ->update(array(
              'password'  => $password,  
          ));
        $request->session()->flash('status', 'Your password send your email successfully');
        return Redirect::to('/forgot-password');
      }else{
        $request->session()->flash('error', 'Invalid credentials');
        return Redirect::to('/forgot-password');
      }
    }



    public function narchive()
    {
      $pages = DB::table('videos')
                ->get();
      $active = "Archive"; 
      return view('admin/narchive',compact('pages','active'));
    }

    public function edit_narchive($id)
    {
      $pages = DB::table('videos')
                ->leftJoin('gallery','gallery.id', '=' ,'videos.gallery_id')
                ->select('videos.*','gallery.title as gallerytitle')
                ->orderBy('videos.id','DESC')
                ->where('videos.id',$id)
                ->first();
      $gallery = DB::table('gallery')
                 ->get();
      $active = "Archive";
        return view('admin/edit_narchive',compact('pages','active','gallery'));
    }

    public function update_narchive(Request $request)
    {
      $id     = $_POST['id'];
      $title  = $_POST['title'];
      $gallery  = $_POST['gallery'];
      //if($title != 'Gallery'){
      $downloading          = $_POST['downloading'];
      $old_video  = $_POST['old_video'];
      $old_image  = $_POST['old_image'];
      $today1 = date("YmdHis"); 
        if(!empty($_FILES['image']['name'])){
            $image = $_FILES['image']['name'];
            $temp_anme = $_FILES["image"]["tmp_name"];
            $ext = pathinfo($image, PATHINFO_EXTENSION);
            $name = "video_".$today1."_".rand(4,99999).".".$ext;
            $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/Jaipuruz/public/gallery/';
            $imagePath = $uploadfile. $name;
            move_uploaded_file($temp_anme,$imagePath);
            @unlink($uploadfile.$old_image);
            $hm_image = $name;
        }else{
            $hm_image = $old_image;
        }

        if(!empty($_FILES['video']['name'])){
          $video = $_FILES['video']['name'];
          $temp_anme1 = $_FILES["video"]["tmp_name"];
          $ext1 = pathinfo($video, PATHINFO_EXTENSION);
          $name12 = "video_".$today1."_".rand(4,99999).".".$ext1;
          $uploadfile1 = $_SERVER['DOCUMENT_ROOT'] . '/Jaipuruz/public/gallery/';
          $imagePath1 = $uploadfile1. $name12;
          move_uploaded_file($temp_anme1,$imagePath1);
          @unlink($uploadfile1.$old_video);
          $hm_image1 = $name12;
        }else{
          $hm_image1 = $old_video;
        }
        $today = date("Y-m-d"); 
        $update=DB::table('videos')
            ->where('id',$id)
            ->update(array(
            'title'           => $title,
            'gallery_id'      => $gallery,
            'photo'           => $hm_image,
            'video'           => $hm_image1,
            'download_satus'  => $downloading,
        ));
      //}
      
      $request->session()->flash('success', 'Your Video has been updated');
      return Redirect::to('/admin/archive');
    }

    public function remove_narchive(Request $request,$id)
    {
      $pages = DB::table('videos')
                ->where('id',$id)
                ->first();
      $uploadfile1 = $_SERVER['DOCUMENT_ROOT'] . '/Jaipuruz/public/gallery/';         
      $image = $pages->photo;
      $video = $pages->video;
      @unlink($uploadfile1.$image);
      @unlink($uploadfile1.$video);
      // $teams = $_POST['clients'];
      // $teams=DB::table('users')->whereIn('id', $teams)->delete();
      //echo "<pre>"; print_r($_POST); die;
      $teams=DB::table('videos')->where('id', $id)->delete();
      $request->session()->flash('success', 'Your Video has been deleted');
      return redirect()->back();
    }
    

}
