<?php 

namespace App\Http\Controllers\API;

use App\Http\Requests\PageFormRequest;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Components\FlashMessages;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Validator;

class WebserviceController extends Controller
{ 
    public function __construct()
    {
      $this->apiToken = uniqid(base64_encode(str_random(60)));
        //$this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    
  //---login- type Post Start//
    public function login(Request $request)
    { 
      if(isset($_POST) && !empty($_POST)){
        $phone     = (@$_POST['phone']) ?: '';
        $today  = date("Y-m-d h:i:sa"); 
        $today1 = date("Y-m-d"); 
        $users  = DB::table('users')
                  ->where('phone', $phone)
                  //->where('status', '1') //-Role 1 user type--//
                  ->first();
        if(isset($users) && !empty($users)) {
          if($users->status == "1"){
            $characters = '01234567890';
            $charactersLength = strlen($characters);
            $password1 = '';
            $length = 6;
            for ($i = 0; $i < $length; $i++) {
                $password1 .= $characters[rand(0, $charactersLength - 1)];
            }
            $update = DB::table('users')
              ->where('phone', $phone)
              ->update(array(
              'otp' => $password1
            ));
            return response()->json([
              'status'  => TRUE,
              'message' => 'OTP send successfully!',
              'otp'    => $password1
            ]);
          }else{
            return response()->json([
              'status'  => FALSE,
              'message' => 'Your Account Not Approve!',
            ]);
          }
        }
        else{
          return response()->json([
            'status'  => FALSE,
            'message' => 'Invalid login credentials!',
          ]);
        }
      }
      else{
        return response()->json([
          'status'  => FALSE,
          'message' => 'Some error found!'
        ]);
      }
    }
  //---login- type Post End//

  //---user Check OPT  type POST Start-//
    public function check_otp(Request $request)
    {
      if(isset($_POST) && !empty($_POST)){
        $phone   = $_POST['phone'];
        $otp      = $_POST['otp'];
        $users  = DB::table('users')
                  ->where('phone', $phone)
                  ->first();
        if(isset($users) && !empty($users)) {
          if($otp == $users->otp){
            return response()->json([
              'status'  => TRUE,
              'message' => 'Otp match successfully!'
            ]);
          }else{
            return response()->json([
              'status'  => FALSE,
              'message' => 'Your not match!',
            ]);
          }
        }
        else{
          return response()->json([
            'status' => FALSE,
            'message' => 'Mobile not found!',
            'otp' => $otp
          ]);
        }
      }
      else{
        return response()->json([
          'status' => FALSE,
          'message' => 'Some error found!'
        ]);
      }
    }
  //---user Check OPT  type POST End-//

  //---video-qrcode- type Post Start//
    public function video_qrcode(Request $request)
    { 
      if(isset($_POST) && !empty($_POST)){
        $video_id     = (@$_POST['video_id']) ?: '';
        $user_id     = (@$_POST['user_id']) ?: '';
        $today  = date("Y-m-d h:i:sa"); 
        $today1 = date("Y-m-d"); 
        $video  = DB::table('videos')
                  ->where('id', $video_id)
                  ->first();
        if(isset($video) && !empty($video)) {
            $watch_video  = DB::table('watch_video')
                            ->where('video_id', $video_id)
                            ->where('user_id', $user_id)
                            ->first();
            if(empty($watch_video)){
              $update = DB::table('watch_video')
                  ->insert(array(
                  'video_id' => $video_id,
                  'user_id' => $user_id,
              ));
            }
            return response()->json([
              'status'  => TRUE,
              'message' => 'Video',
              'data'    => $video,
            ]);
        }
        else{
          return response()->json([
            'status'  => FALSE,
            'message' => 'Video not found!',
          ]);
        }
      }
      else{
        return response()->json([
          'status'  => FALSE,
          'message' => 'Some error found!'
        ]);
      }
    }
  //---video-qrcode- type Post End//

  //---video-download- type Post Start//
    public function video_download(Request $request)
    { 
      if(isset($_POST) && !empty($_POST)){
        $gallery_id = (@$_POST['gallery_id']) ?: '';
        $user_id    = (@$_POST['user_id']) ?: '';
        $today      = date("Y-m-d h:i:s"); 
        $today1     = date("Y-m-d"); 
        $video      = DB::table('videos')
                      ->where('gallery_id', $gallery_id)
                      ->where('download_satus','1')
                      ->get();
        if(isset($video) && !empty($video)) {
          $videos      = DB::table('videos')
                      ->where('gallery_id', $gallery_id)
                      ->where('download_satus','1')
                      ->count();
            // $download_video  = DB::table('download_video')
            //                 ->where('video_id', $video_id)
            //                 ->where('user_id', $user_id)
            //                 ->first();
            //if(empty($download_video)){
              $update = DB::table('download_video')
                  ->insert(array(
                  'gallery_id'=> $gallery_id,
                  'video'     => $videos,
                  'user_id'   => $user_id,
                  'created_at' => $today
              ));
            //}
            return response()->json([
              'status'  => TRUE,
              'message' => 'Video download',
              'videos'    => $videos,
              'data'    => $video
            ]);
        }
        else{
          return response()->json([
            'status'  => FALSE,
            'message' => 'Video not found!',
          ]);
        }
      }
      else{
        return response()->json([
          'status'  => FALSE,
          'message' => 'Some error found!'
        ]);
      }
    }
  //---video-download- type Post End//

  //---video-download- type Post Start//
    public function downloaded_list(Request $request)
    { 
      if(isset($_POST) && !empty($_POST)){
        $user_id    = (@$_POST['user_id']) ?: '';
        $download   = DB::table('download_video')
                      ->leftJoin('gallery','gallery.id', '=' ,'download_video.gallery_id')
                      ->select('download_video.*','gallery.title')
                      ->where('download_video.user_id', $user_id)
                      ->get();
        if(isset($download) && !empty($download)) {
            return response()->json([
              'status'  => TRUE,
              'message' => 'Downloaded video list',
              'data'    => $download
            ]);
        }
        else{
          return response()->json([
            'status'  => FALSE,
            'message' => 'Download not found!',
          ]);
        }
      }
      else{
        return response()->json([
          'status'  => FALSE,
          'message' => 'Some error found!'
        ]);
      }
    }
  //---video-download- type Post End//

  //---help- type GET Start//
    public function help()
    { 
      $helping_customers  = DB::table('helping_customers')
                ->first();
      if(isset($helping_customers) && !empty($helping_customers)) {
          return response()->json([
            'status'  => TRUE,
            'message' => 'Help',
            'data'    => $helping_customers,
          ]);
      }
      else{
        return response()->json([
          'status'  => FALSE,
          'message' => 'Help not found!',
        ]);
      }
    }
  //---help- type GET End//

  //---help- type GET Start//
    public function instructions()
    { 
      $instructions  = DB::table('instructions')
                ->first();
      if(isset($instructions) && !empty($instructions)) {
          return response()->json([
            'status'  => TRUE,
            'message' => 'Instructions',
            'data'    => $instructions,
          ]);
      }
      else{
        return response()->json([
          'status'  => FALSE,
          'message' => 'Instructions not found!',
        ]);
      }
    }
  //---help- type GET End//







//--old--Function--//
  //---forgot password- type Post Start//
    public function forgot_password(Request $request)
    {
      if(isset($_POST) && !empty($_POST)){
        $email  = $_POST['email'];
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $password1 = '';
        $length = 8;
        for ($i = 0; $i < $length; $i++) {
            $password1 .= $characters[rand(0, $charactersLength - 1)];
        }
        $password = Hash::make($password1);
        $users= DB::table('users')
                  ->where('email', $email)
                  ->where('status', '1')
                  ->first();
        $admin= DB::table('admin')
                  ->where('role', '1')
                  ->first();
        if(isset($users) && !empty($users)){
            $site =  config('app.name');
            // echo "<pre>"; print_r($users); die;
            $forgot_password = DB::table('templates')
              ->where('temp_key', 'forgot_password')
              ->first();
            $user_sub     = $forgot_password->temp_subject;
            $user_text    = $forgot_password->temp_text;
            $user_text = str_replace("{user_name}",$users->name,$user_text);
            $user_text = str_replace("{site_title}",$site,$user_text);
            $user_text = str_replace("{password}",$password1,$user_text);
            //echo $user_text;
            $to = $email;
            $subject = $user_sub;
            $from = $admin->email;
            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: '.$from."\r\n".
                    'Reply-To: '.$from."\r\n" .
                    'X-Mailer: PHP/' . phpversion();
            mail($to, $subject, $user_text, $headers);
            $update=DB::table('users')
                ->where('id', $users->id)
                ->update(array(
                'password'  => $password,  
            ));
          return response()->json([
            'status' => TRUE,
            'message' => 'Password send you Email-Address Successfully!',
            'data' => $password1
          ]);
        }else{
          return response()->json([
            'status' => FALSE,
            'message' => 'Invalid Email Address!',
          ]);
        }
      }
      else{
        return response()->json([
          'status' => FALSE,
          'message' => 'Some error found!'
        ]);
      }
    }
  //---forgot password- type Post End//

  //---user profile- type POST Start//
    public function profile(Request $request)
    {
      $token = $request->header('Authorization');
      if(isset($token) && !empty($token)){
        if(isset($_POST) && !empty($_POST)){
          $user_id  = (@$_POST['user_id']) ?: '';
          $type     = (@$_POST['type']) ?: '';
          $users = DB::table('users')
              ->where('id', $user_id)
              ->where('role', $type)
              ->where('api_token', $token)
              ->first();
          if(!empty($users)){
            $response = array();
            $response['id']     = $users->id;
            $response['name']   = (@$users->name) ?: '';
            $response['email']  = (@$users->email) ?: '';
            $response['phone']  = (@$users->phone) ?: '';
            $response['image']  = (@$users->image) ?: '';
            $response['city']   = (@$users->city) ?: '';
            $response['address']      = (@$users->address) ?: '';
            $response['access_token'] = (@$users->apiToken) ?: '';
            return response()->json([
                'status' => TRUE,
                'message' => 'User profile',
                'data' => $response
            ]);
          }else{
             return response()->json([
              'status' => FALSE,
              'message' => 'User not found!'
            ]);
          }
        }else{
          return response()->json([
              'status' => FALSE,
              'message' => 'Some error found !'
          ]);
        }
      }else{
        return response()->json([
          'status' => FALSE,
          'message' => 'Not a valid API request!',
        ]);
      }
    }
  //---user profile- type POST End//

  

//old functions-- //

  //---user update-profile -type POST Start//
    public function update_profile(Request $request)
    {
      $token = $request->header('Authorization');
      if(isset($token) && !empty($token)){  
        if(isset($_POST) && !empty($_POST)){
          $today    = date("Y-m-d"); 
          $id       = (@$_POST['user_id']) ?: '';
          $name1    = (@$_POST['name']) ?: '';
          $email    = (@$_POST['email']) ?: '';
          $age      = (@$_POST['age']) ?: '';
          $gender   = (@$_POST['gender']) ?: '';
          $address  = (@$_POST['address']) ?: '';
          $users  = DB::table('users')
                    ->where('id', $id)
                    ->where('api_token', $token)
                    ->where('status', '1')
                    ->first();
          if(isset($users) && !empty($users)) {
            $usercheck = DB::table('users')
                  ->where('email', $email)
                  ->where('id', '!=', $id)
                  ->first();
            if(empty($usercheck)){
              $oldpicture    = (@$_POST['old_image']) ?: '';
                if(!empty($_FILES['image']['name'])){
                  $image = $_FILES['image']['name'];
                  $temp_anme = $_FILES["image"]["tmp_name"];
                  $ext = pathinfo($image, PATHINFO_EXTENSION);
                  $name = rand(10,99999).".".$ext;
                  $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/Jaipuruz/public/users/';
                  $imagePath = $uploadfile. $name;
                  move_uploaded_file($temp_anme,$imagePath);
                  @unlink($uploadfile.$oldpicture);
                  $hm_image = $name;
                }else{
                  $hm_image = $oldpicture;
                }
                $update=DB::table('users')
                  ->where('id', $id)
                  ->update(array(
                  'name'        =>  $name1,
                  'email'       =>  $email,
                  'age'         =>  $age,
                  'gender'      =>  $gender,
                  'address'     =>  $address,
                  'image'       =>  $hm_image,
                  'updated_at'  =>  $today
                ));
                $userdata = DB::table('users')
                    ->where('id', $id)
                    ->first();
                return response()->json([
                  'status' => TRUE,
                  'message' => 'Update Successfully!',
                  'data' => $userdata
                ]);
            }else{
              return response()->json([
                'status' => FALSE,
                'message' => 'Email already exist!'
              ]);
            }
          }else{
            return response()->json([
              'status' => FALSE,
              'message' => 'User not found!'
            ]);
          }
        }else{
          return response()->json([
              'status' => FALSE,
              'message' => 'Some error found !'
          ]);
        }
      }else{
        return response()->json([
          'status' => FALSE,
          'message' => 'Not a valid API request!',
        ]);
      }
    }
  //---user update-profile -type POST End//

  //---user-delete- type GET//
    public function delete_profile($user_id){
        header('Access-Control-Allow-Origin: *');
        // ALLOW OPTIONS METHOD
        $headers = [
            'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Headers' => 'Content-Type, X-Auth-Token, Origin, Authorization'
        ];
        if(isset($user_id) && !empty($user_id)){ 
          $delete1=DB::table('users')->where('id', $user_id)->delete();
          return response()->json([
              'status' => '200',
              'message' => 'User delete Successfully'
          ]);
        }
        else{
          return response()->json([
              'status' => '201',
              'message' => 'Some error found!'
          ]);
        }
    }
  //---user-delete- type GET//

}

