<?php 

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\APIToken as Middleware;

class APIToken extends Middleware
{

	public function handle($request, Closure $next)
	{
	  if($request->header('Authorization')){
	    return $next($request);
	  }
	  return response()->json([
	    'message' => 'Not a valid API request.',
	  ]);
	}

}